//
//  OptionsVC.h
//  Ques2Learn
//
//  Created by apple on 2/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol OptionsVCDelegate
- (void)rowOptionSelected:(NSString *)selectedStr;
@end
@interface OptionsVC : UIViewController <UIPickerViewDelegate,UIPickerViewDataSource,UIPopoverControllerDelegate>{
    NSMutableArray *_arrOptions;
    id<OptionsVCDelegate> _delegate;
    NSInteger tagRow;
    UIPopoverController *popV;
    DAL *objDAL;
    NSString *strStudID;
}
@property (nonatomic, retain) NSMutableArray *arrOptions;
@property (nonatomic, retain) id<OptionsVCDelegate> delegate;
-(void)btnSelectOp:(id)sender;
-(void)clickDone:(id)sender;
-(void)getOptions:(NSMutableArray*)arrGet:(NSString*)idStr;
@end
