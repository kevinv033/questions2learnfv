//
//  AddStudentVC.m
//  Ques2Learn
//
//  Created by apple on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AddStudentVC.h"
#import "MainViewAppDelegate.h"

#include <stdlib.h>

@implementation AddStudentVC
@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    objDAL = [[DAL alloc] initDatabase:@"Que2learn.sqlite"];
    if (updateTag==2) {
        btnDelete.hidden = NO;
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"select * from StudSettings where StudID='%@'",strStudID]]];
        if ([dict count]>0) {
            txtName.text=[[dict valueForKey:@"Table1"] valueForKey:@"StudName"];
            txtAge.text=[[dict valueForKey:@"Table1"] valueForKey:@"StudAge"];
            txtGrade.text=[[dict valueForKey:@"Table1"] valueForKey:@"StudGrade"];
            strSName = [[dict valueForKey:@"Table1"] valueForKey:@"StudPhoto"];
            strSN = [[dict valueForKey:@"Table1"] valueForKey:@"StudPhoto"];
            if ([strSN isEqualToString:@"OK.png"]) {
            }else {
                NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *docsDir = [dirPaths objectAtIndex:0];
                NSString *imgFilePath = [docsDir stringByAppendingPathComponent:strSN];
                UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
                [btnStudPhoto setImage:imgGet forState:UIControlStateNormal];
            }
            strSN =@"";
        }
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
#pragma -
#pragma UITextField delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    obj = textField;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string { 
    if (textField==txtAge) {
        if ([string isEqualToString:@""]) {
            return YES;
        }
        NSCharacterSet *nonNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0);
    }
    return YES;
}
#pragma mark -
#pragma mark UIImagePickerDelegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	[picker dismissViewControllerAnimated:YES completion:nil];
    [popover dismissPopoverAnimated:YES];
    if (updateTag == 2) {
        strSN = @"1";
    }
    UIImage *img = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    if (!img) {
        NSLog(@"No image");
    }
    UIImage *imgCopy = [self rotateImage:img];
    [btnStudPhoto setImage:imgCopy forState:UIControlStateNormal];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    //Pick a default image if the picker is cancelled
    int r = arc4random_uniform(11);
    NSString *avatar = [NSString stringWithFormat:@"avatar-%i", r];
    [btnStudPhoto setImage:[UIImage imageNamed:avatar] forState:UIControlStateNormal];
    
    [picker dismissModalViewControllerAnimated:YES];
    [popover dismissPopoverAnimated:YES];
}
#pragma
#pragma mark - UIAlertview delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  //Handle alert view for photo capture
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                pickerPhoto = [[UIImagePickerController alloc] init];
                pickerPhoto.sourceType =  UIImagePickerControllerSourceTypeCamera;
                pickerPhoto.delegate = self;
                //Instead of popover, use modal.
//                pickerPhoto.modalPresentationStyle = UIModalPresentationFullScreen;
                [self presentViewController:pickerPhoto animated:YES completion:nil];
                pickerPhoto.view.frame = CGRectMake(0.0, 0.0, 1024, 768); //Need to hardcode frame :(

            }else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your device not compatible to take photo" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
          
        }else if (buttonIndex==1){
            pickerPhoto = [[NonRotatingUIImagePickerController alloc] init];
            pickerPhoto.delegate = self;
            if ([UIImagePickerController isSourceTypeAvailable:
                 UIImagePickerControllerSourceTypePhotoLibrary]) 
            {
                pickerPhoto.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
                
            }
            popover  = [[UIPopoverController alloc] initWithContentViewController:pickerPhoto];
            [popover  presentPopoverFromRect:CGRectMake(-220, -95, 320, 445) inView:self.view permittedArrowDirections:4 animated:YES];
        }
        MainViewAppDelegate *appdel = (MainViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        appdel.obj = popover;
    }
  //  Handle alert view for deletion
  if (alertView.tag == 100) {
    if (buttonIndex == 1) {
      [self deleteStudent];
    }
  }
}
#pragma
#pragma mark - custom methods
-(UIImage *)rotateImage:(UIImage *)image {    
    
    int kMaxResolution = 640;
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
    
} 
-(void)getStudentDat:(NSString*)strStudID1:(NSInteger)tagUpdate {
    strStudID=strStudID1;
    updateTag = tagUpdate;
}
-(void)clickDone:(id)sender
{
    if (updateTag==2) {
        if ([strSN isEqualToString:@"1"]) {
            if ([strSName isEqualToString:@"OK.png"]) {
                // check if image set
                NSString *strStuImg=@"";
        // TODO: If no image for Student store a default image
                if ([btnStudPhoto imageForState:UIControlStateNormal]) {
                    strStuImg = [NSString stringWithFormat:@"%@.png",[NSDate date]];
                    NSLog(@"Yes");
                    NSData *imageData = UIImagePNGRepresentation([btnStudPhoto imageForState:UIControlStateNormal]);
                    if (imageData == nil) {
//                        strStuImg = @"OK.png";
                        int r = arc4random_uniform(11);
                        
                        NSString *avatar = [NSString stringWithFormat:@"avatar-%i", r];
                        strStuImg = [avatar stringByAppendingPathExtension:@"png"];
                        NSString *imgPath = [[NSBundle mainBundle] pathForResource:avatar ofType:@"png"];
                        NSData *imgData = [NSData dataWithContentsOfFile:imgPath];
                        if (imgData) {
                            NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            NSString *docsDir = [dirPaths objectAtIndex:0];
                            NSString *imgFilePath = [docsDir stringByAppendingPathComponent:strStuImg];
                            [[NSFileManager defaultManager] createFileAtPath:imgFilePath contents:imgData attributes:nil];
                        }
                    }else {
                         NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString *docsDir = [dirPaths objectAtIndex:0];
                        NSString *imgFilePath = [docsDir stringByAppendingPathComponent:strStuImg];
                        [[NSFileManager defaultManager] createFileAtPath:imgFilePath contents:imageData attributes:nil];
                    }
                }else{
//                    strStuImg = @"OK.png";
                    int r = arc4random_uniform(11);
                    
                    NSString *avatar = [NSString stringWithFormat:@"avatar-%i", r];
                    strStuImg = [avatar stringByAppendingPathExtension:@"png"];
                    NSString *imgPath = [[NSBundle mainBundle] pathForResource:avatar ofType:@"png"];
                    NSData *imgData = [NSData dataWithContentsOfFile:imgPath];
                    if (imgData) {
                        NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString *docsDir = [dirPaths objectAtIndex:0];
                        NSString *imgFilePath = [docsDir stringByAppendingPathComponent:strStuImg];
                        [[NSFileManager defaultManager] createFileAtPath:imgFilePath contents:imgData attributes:nil];
                    }

                    NSLog(@"No");
                }
                //..
                NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:txtName.text,@"StudName",txtAge.text,@"StudAge",txtGrade.text,@"StudGrade",strStuImg,@"StudPhoto", nil];
                [objDAL updateRecord:dict forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStudID]];
            }else {
                NSString *strStuImg = strSName;
                NSData *imageData = UIImagePNGRepresentation([btnStudPhoto imageForState:UIControlStateNormal]);
                if (imageData == nil) {
                }else {
                    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *docsDir = [dirPaths objectAtIndex:0];
                    NSString *imgFilePath = [docsDir stringByAppendingPathComponent:strStuImg];
                    [[NSFileManager defaultManager] createFileAtPath:imgFilePath contents:imageData attributes:nil];
                }
                NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:txtName.text,@"StudName",txtAge.text,@"StudAge",txtGrade.text,@"StudGrade", nil];
                [objDAL updateRecord:dict forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStudID]];
            }
        }else {
            NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:txtName.text,@"StudName",txtAge.text,@"StudAge",txtGrade.text,@"StudGrade", nil];
            [objDAL updateRecord:dict forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStudID]];        
        }
        [_delegate AddStudentDone:1];
    }else{
        // set if grouprd or not.
        NSString *str =@"";
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2) {
            if ([[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT COUNT(StudGroup) FROM StudSettings WHERE StudGroup='ON'"]] valueForKey:@"Table1"] valueForKey:@"COUNT(StudGroup)"] isEqualToString:@"4"]) {
                str = @"OFF";
            }else{
                str = @"ON";
            }
        }else{
            str = @"OFF";
        }
        //..
        // check if image set
        NSString *strStuImg=@"";
        if ([btnStudPhoto imageForState:UIControlStateNormal]) {
            strStuImg = [NSString stringWithFormat:@"%@.png",[NSDate date]];
            NSLog(@"Yes");
            NSData *imageData = UIImagePNGRepresentation([btnStudPhoto imageForState:UIControlStateNormal]);
            if (imageData == nil) {
//                strStuImg = @"OK.png";
                int r = arc4random_uniform(11);
                
                NSString *avatar = [NSString stringWithFormat:@"avatar-%i", r];
                strStuImg = [avatar stringByAppendingPathExtension:@"png"];
                NSString *imgPath = [[NSBundle mainBundle] pathForResource:avatar ofType:@"png"];
                NSData *imgData = [NSData dataWithContentsOfFile:imgPath];
                if (imgData) {
                    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *docsDir = [dirPaths objectAtIndex:0];
                    NSString *imgFilePath = [docsDir stringByAppendingPathComponent:strStuImg];
                    [[NSFileManager defaultManager] createFileAtPath:imgFilePath contents:imgData attributes:nil];
                }

            }else {
                NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *docsDir = [dirPaths objectAtIndex:0];
                NSString *imgFilePath = [docsDir stringByAppendingPathComponent:strStuImg];
                [[NSFileManager defaultManager] createFileAtPath:imgFilePath contents:imageData attributes:nil];
            }
        }else{
//            strStuImg = @"OK.png";
            int r = arc4random_uniform(11);
            
            NSString *avatar = [NSString stringWithFormat:@"avatar-%i", r];
            strStuImg = [avatar stringByAppendingPathExtension:@"png"];
            NSString *imgPath = [[NSBundle mainBundle] pathForResource:avatar ofType:@"png"];
            NSData *imgData = [NSData dataWithContentsOfFile:imgPath];
            if (imgData) {
                NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *docsDir = [dirPaths objectAtIndex:0];
                NSString *imgFilePath = [docsDir stringByAppendingPathComponent:strStuImg];
                [[NSFileManager defaultManager] createFileAtPath:imgFilePath contents:imgData attributes:nil];
            }

            NSLog(@"No");
        }
        //..
        // insert record
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:str,@"StudGroup",txtName.text,@"StudName",txtAge.text,@"StudAge",txtGrade.text,@"StudGrade",[NSString stringWithFormat:strStuImg],@"StudPhoto",@"Level 1",@"Level",@"ON",@"VPrompt",@"ON",@"AFeedback",@"Both",@"ReinType", @"Continuous",@"ReinSchedule",@"all",@"QueCat",@"all",@"QueType",@"ON",@"SpokenVoice",@"ON",@"VisualTrials",@"10",@"NoOfTrials",@"3",@"ReinInterval",nil];
        [objDAL insertRecord:dict inTable:@"StudSettings"];
//        NSMutableDictionary *dict1 =  [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT LAST_INSERT_ROWID();"]]];
//        NSLog(@"dict1:%@",[[dict1 valueForKey:@"Table1"] valueForKey:@"LAST_INSERT_ROWID()"]);
        //..
        [_delegate AddStudentDone:1];
    }
}
-(void)clickDelete:(id)sender
{
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Questions2Learn" message:@"Are you sure you want to delete this student?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
  alert.tag = 100;
  [alert show];
}

-(void)deleteStudent {
  [self deleteImageFile:strSName];
  [objDAL deleteFromTable:@"StudSettings" WhereField:@"StudID=" Condition:[NSString stringWithFormat:@"'%@'",strStudID]];
  [objDAL deleteFromTable:@"tblScore" WhereField:@"StudID=" Condition:[NSString stringWithFormat:@"'%@'",strStudID]];
  [_delegate AddStudentDone:2];
}

-(void)deleteImageFile:(NSString*)strFile
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *docsDir = [dirPaths objectAtIndex:0];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *imgSImg = [docsDir stringByAppendingPathComponent:strFile];
	UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgSImg]];		
	if (imgGet==NULL) {
	}else {
		[fileManager removeItemAtPath:imgSImg error:NULL];		
	}
}
-(void)clickChoosePhoto:(id)sender
{
    [obj resignFirstResponder];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Take photo" message:@"From" delegate:self cancelButtonTitle:@"Camera" otherButtonTitles:@"Photos library", nil];
    alert.tag = 1;
    [alert show];
}
@end
