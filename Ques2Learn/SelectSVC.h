//
//  SelectSVC.h
//  Ques2Learn
//
//  Created by apple on 5/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SelectSVCDelegate
- (void)selectStudsDone:(NSString*)idStr;
@end
@interface SelectSVC : UIViewController {
    id <SelectSVCDelegate> delegate;
    NSString *strID;
    IBOutlet UITableView *tblS;
    NSMutableArray *arrStud;
    NSMutableArray *arrGroup;
    NSMutableArray *arrCollect;
    DAL *objDAL;
}
@property (nonatomic,retain) id <SelectSVCDelegate> delegate;
-(void)getStudID:(NSString*)idStr;
-(IBAction)clickDone:(id)sender;
@end
