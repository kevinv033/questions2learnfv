//
//  ModifyPreProgVC.m
//  Ques2Learn
//
//  Created by apple on 5/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ModifyPreProgVC.h"
#import "ModifyVC.h"

@implementation ModifyPreProgVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    tagLevel = 1;
    idQue = 0;
//    Init all arrays
    allQue = [[NSMutableArray alloc] initWithCapacity:0];
    arrQue = [[NSMutableArray alloc] initWithCapacity:0];
    allCat = [[NSMutableArray alloc] initWithCapacity:0];
    arrCat = [[NSMutableArray alloc] initWithCapacity:0];
    schoolQue = [[NSMutableArray alloc] initWithCapacity:0];
    homeQue = [[NSMutableArray alloc] initWithCapacity:0];
    foodQue = [[NSMutableArray alloc] initWithCapacity:0];
    communityQue = [[NSMutableArray alloc] initWithCapacity:0];
    leisureQue = [[NSMutableArray alloc] initWithCapacity:0];
    healthQue = [[NSMutableArray alloc] initWithCapacity:0];
    
    objDAL = [[DAL alloc] initDatabase:@"Que2learn.sqlite"];
    UIButton *btn = (UIButton*)[self.view viewWithTag:1];
    btn.layer.cornerRadius = 2;
    btn.layer.borderWidth = 2;

    UIButton *btn1 = (UIButton*)[self.view viewWithTag:2];
    btn1.layer.cornerRadius = 2;
    btn1.layer.borderWidth = 2;

    UIButton *btn2 = (UIButton*)[self.view viewWithTag:3];
    btn2.layer.cornerRadius = 2;
    btn2.layer.borderWidth = 2;

    // Do any additional setup after loading the view from its nib.

    // Init data here?
    
    NSMutableArray *arr = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:[NSString stringWithFormat:@"tblLevel%d",tagLevel]]];
    
    for (int i=0; i<[arr count]; i++) {
        NSDictionary *di = [NSDictionary dictionaryWithObjectsAndKeys:[[arr objectAtIndex:i] valueForKey:@"id"],@"id",[[arr objectAtIndex:i] valueForKey:@"QText"],@"que", [[arr objectAtIndex:i] valueForKey:@"QCat"], @"category",nil];
        
        //Check if category is in arrCat only need to add once
        if (![allCat containsObject:[[arr objectAtIndex:i] valueForKey:@"QCat"]]) {
            
            [allCat addObject:[[arr objectAtIndex:i] valueForKey:@"QCat"]];
        }
        //Add item to question array
        [allQue addObject:di];
    
    }
    [allCat insertObject:@"All" atIndex:0];

    // Separate Arrays for categories
    for (NSDictionary *item in allQue) {
        if ([[item valueForKey:@"category"] isEqualToString:@"School"]) {
            [schoolQue addObject:item];
        }
        if ([[item valueForKey:@"category"] isEqualToString:@"Home"]) {
            [homeQue addObject:item];
        }
        if ([[item valueForKey:@"category"] isEqualToString:@"Food & Drinks"]) {
            [foodQue addObject:item];
        }
        if ([[item valueForKey:@"category"] isEqualToString:@"Community"]) {
            [communityQue addObject:item];
        }
        if ([[item valueForKey:@"category"] isEqualToString:@"Leisure/Recreational"]) {
            [leisureQue addObject:item];
        }
        if ([[item valueForKey:@"category"] isEqualToString:@"Health"]) {
            [healthQue addObject:item];
        }
    }
    
    tbl.delegate=self;
    tbl.dataSource=self;
    
    [self reloadTable]; // This is where the data gets init currently
    
    // Init button for category selection
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"All Categories: %@", allCat);
    NSLog(@"All Questions: %@", foodQue);
#endif
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [arrCat count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [arrCat objectAtIndex:section];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    // Get Category String from arrCat
    NSString *category = [arrCat objectAtIndex:(NSUInteger) section];
    NSMutableArray *rowsInSectionArray = [[NSMutableArray alloc] initWithCapacity:0];
    //Enumeration
    for (NSDictionary *item in allQue) {
        if ([[item valueForKey:@"category"] isEqualToString:category]) {
            [rowsInSectionArray addObject: item];
        }
    }
    
    return [rowsInSectionArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    NSArray *questionArray;
    
    if ([arrCat count] > 1) {
        switch (indexPath.section) {
            case 1:
                questionArray = schoolQue;
                break;
            case 2:
                questionArray = homeQue;
                break;
            case 3:
                questionArray = foodQue;
                break;
            case 4:
                questionArray = communityQue;
                break;
            case 5:
                questionArray = leisureQue;
                break;
            case 6:
                questionArray = healthQue;
                break;
            default:
                break;

        }
    }else{
        if ([[arrCat objectAtIndex:0] isEqualToString:@"School"]) {
            questionArray = schoolQue;
        }
        if ([[arrCat objectAtIndex:0] isEqualToString:@"Home"]) {
            questionArray = homeQue;
        }
        if ([[arrCat objectAtIndex:0] isEqualToString:@"Food & Drinks"]) {
            questionArray = foodQue;
        }
        if ([[arrCat objectAtIndex:0] isEqualToString:@"Community"]) {
            questionArray = communityQue;
        }
        if ([[arrCat objectAtIndex:0] isEqualToString:@"Health"]) {
            questionArray = healthQue;
        }
        if ([[arrCat objectAtIndex:0] isEqualToString:@"Leisure/Recreational"]) {
            questionArray = leisureQue;
        }
    }
    
    NSString *question = [[questionArray objectAtIndex:indexPath.row] valueForKey:@"que"];
    cell.textLabel.text = question;
#ifdef DEBUGX
        NSLog(@"Category: %i", indexPath.section);
#endif
    return cell;
    
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSArray *questionArray; //Array for categorized questions
    
    if ([arrCat count] > 1) {
        switch (indexPath.section) {
            case 1:
                questionArray = schoolQue;
                break;
            case 2:
                questionArray = homeQue;
                break;
            case 3:
                questionArray = foodQue;
                break;
            case 4:
                questionArray = communityQue;
                break;
            case 5:
                questionArray = leisureQue;
                break;
            case 6:
                questionArray = healthQue;
                break;
            default:
                break;
                
        }
    }else{
        if ([[arrCat objectAtIndex:0] isEqualToString:@"School"]) {
            questionArray = schoolQue;
        }
        if ([[arrCat objectAtIndex:0] isEqualToString:@"Home"]) {
            questionArray = homeQue;
        }
        if ([[arrCat objectAtIndex:0] isEqualToString:@"Food & Drinks"]) {
            questionArray = foodQue;
        }
        if ([[arrCat objectAtIndex:0] isEqualToString:@"Community"]) {
            questionArray = communityQue;
        }
        if ([[arrCat objectAtIndex:0] isEqualToString:@"Health"]) {
            questionArray = healthQue;
        }
        if ([[arrCat objectAtIndex:0] isEqualToString:@"Leisure/Recreational"]) {
            questionArray = leisureQue;
        }
        
        
    }
    idQue = [[[questionArray objectAtIndex:indexPath.row] valueForKey:@"id"] intValue];
    
    //idQue = [[[arrQue objectAtIndex:indexPath.row] valueForKey:@"id"] intValue];
}
#pragma mark - Custom methods

-(void)getStudID:(NSString*)idStr
{
    strStudID = idStr;
}
-(void)clickHome:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickSelectCategory:(id)sender {
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    
    UIButton *senderButton = (UIButton *)sender;
    
    catView = [CategoryTableViewController new];
//    catView.tableView.dataSource = ;
    catView.popoverDelegate = self;
    [catView setCategoryArray:allCat];
    popover = [[UIPopoverController alloc] initWithContentViewController:catView];
    popover.delegate = self;
    popover.popoverContentSize = CGSizeMake(200, 315);
    [popover presentPopoverFromRect:senderButton.bounds inView:senderButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void)reloadTable {
    [self reloadTableWithCategory:@"All"];
}

-(void)reloadTableWithCategory:(NSString *)category
{
    //Populate array with questions
    // Empty the arrQue
    [arrCat removeAllObjects];
    [arrQue removeAllObjects];
  
    if (![category isEqualToString:@"All"]) {
        
        [arrCat addObject:category];
        
        for (int i = 0; i < [allQue count]; i++) {
            if ([category isEqualToString:[[allQue objectAtIndex:i] valueForKey:@"category" ]]) {
                [arrQue addObject:[allQue objectAtIndex:i]];
            }
        }
    }else{
        arrCat = (NSMutableArray *)[allCat mutableCopy];
        arrQue = (NSMutableArray *)[allQue mutableCopy];
    }
    
    [tbl reloadData];
    [tbl scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES]; //UITableView scroll to top.
}

- (void)popOverItemSelected:(NSString *)selectedItem {
    
    [popover dismissPopoverAnimated:YES];
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"Selected Category: %@", selectedItem);
#endif
    //Reload Table with new Category.
    //Change button label to match.
    
    [self reloadTableWithCategory:selectedItem];
    UIButton *catButton = (UIButton *)[self.view viewWithTag:100];
    [catButton setTitle:selectedItem forState:UIControlStateNormal];
}


-(void)clickChooseLevel:(id)sender
{
    UIButton *btn = sender;
//    btn.layer.borderColor = [UIColor blackColor].CGColor;
    btn.selected = YES;
    
    switch (btn.tag) {
        case 1:
        {
            tagLevel=1;
            UIButton *btn1 = (UIButton*)[self.view viewWithTag:2];
//            btn1.layer.borderColor = [UIColor lightGrayColor].CGColor;
            UIButton *btn2 = (UIButton*)[self.view viewWithTag:3];
//            btn2.layer.borderColor = [UIColor lightGrayColor].CGColor;
            btn1.selected = btn2.selected = NO;

        }
            break;
        case 2:
        {
            tagLevel=2;
            UIButton *btn1 = (UIButton*)[self.view viewWithTag:1];
//            btn1.layer.borderColor = [UIColor lightGrayColor].CGColor;
            UIButton *btn2 = (UIButton*)[self.view viewWithTag:3];
//            btn2.layer.borderColor = [UIColor lightGrayColor].CGColor;
            btn1.selected = btn2.selected = NO;

        }
            break;
        case 3:
        {
            tagLevel=3;
            UIButton *btn1 = (UIButton*)[self.view viewWithTag:1];
//            btn1.layer.borderColor = [UIColor lightGrayColor].CGColor;
            UIButton *btn2 = (UIButton*)[self.view viewWithTag:2];
//            btn2.layer.borderColor = [UIColor lightGrayColor].CGColor;
            btn1.selected = btn2.selected = NO;

        }
            break;
        default:
            break;
    }
    
    //[self reloadTable];
}
-(void)clickModify:(id)sender
{
    if (idQue==0 && tagLevel==0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Question and Level are not selected" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }else if (idQue==0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Question is not selected" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }else if (tagLevel==0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Level is not selected" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    ModifyVC *viewController = [[ModifyVC alloc] initWithNibName:nil bundle:nil];
    [viewController getID:idQue :tagLevel:strStudID];
    [self.navigationController pushViewController:viewController animated:YES];    
}

-(void)openDirections:(id)sender
{
    UIViewController *vc = [[UIViewController alloc] init];
    vc.modalPresentationStyle = UIModalPresentationFormSheet;
    UIWebView *webView = [[UIWebView alloc] init];

    UINavigationBar *navbar = [[UINavigationBar alloc] init];
    UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)];
    UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:@"Directions for Modifying Pictures"];
    item.rightBarButtonItem = close;
    [navbar pushNavigationItem:item animated:NO];

    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"directions" ofType:@"html" inDirectory:@"html"] isDirectory:NO]]];
    [vc.view addSubview:webView];
    [vc.view addSubview:navbar];
    
    [self presentViewController:vc animated:YES completion:nil];
    //Set the frames based on the modal style
    webView.frame = vc.view.frame;
    navbar.frame = CGRectMake(0.0f, 0.0f, vc.view.frame.size.width, 48.0f);
    navbar.barStyle = UIBarStyleBlack;
    CGRect frame = webView.frame;
    frame.origin.y += navbar.frame.size.height;
    webView.frame = frame;

}
-(void)close
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
