//
//  ModifyVC.m
//  Ques2Learn
//
//  Created by apple on 5/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ModifyVC.h"

@implementation ModifyVC
@synthesize viewSVC = _viewSVC;
@synthesize popOverOVC = _popOverOVC;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    corAns=0;
    tagOptions=0;
    popDir = 0;
//    arrPhotos = [[NSMutableArray alloc] init];
    dS = [[NSMutableDictionary alloc] init];
       NSString *strModify = @"";
    objDAL = [[DAL alloc] initDatabase:@"Que2learn.sqlite"];
    NSLog(@"Count:%@",[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT COUNT(id) FROM tblModify where Level='%d' and StudID='%@' and id='%d'",idLevel,strStudID,idQue]]);
    if ([[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT COUNT(id) FROM tblModify where Level='%d' and StudID='%@' and id='%d'",idLevel,strStudID,idQue]] valueForKey:@"Table1"] valueForKey:@"COUNT(id)"] isEqualToString:@"0"]) {
        dictQue = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"select * from tblLevel%d where id=%d",idLevel,idQue]]];
        strModify = @"0,0,0,0,0";
    }else {
        dictQue = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"select * from tblModify where Level='%d' and id='%d' and StudID='%@'",idLevel,idQue,strStudID]]];
        strModify = [[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"];
    }

    txtQue.text = [[dictQue valueForKey:@"Table1"] valueForKey:@"QText"];
    NSArray *arrSepAns = [[[[dictQue valueForKey:@"Table1"] valueForKey:@"CorrectAns"] stringByReplacingOccurrencesOfString:@" " withString:@""] componentsSeparatedByString:@"&"];
    [self loadQueView];
    switch (idLevel) {
        case 1:
        {
            if ([[arrSepAns objectAtIndex:0] isEqualToString:@"E"]) {
                [self setCorrectAns:btnCorAns1];
            }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"F"]) {
                [self setCorrectAns:btnCorAns2];
            }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"G"]) {
                [self setCorrectAns:btnCorAns3];
            }
            if ([[[strModify componentsSeparatedByString:@","] objectAtIndex:0] isEqualToString:@"0"]) {
                tagModify1=0;
                [btnAns1 setImage:[UIImage imageNamed:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans1Img"]] forState:UIControlStateNormal];
            }else {
                tagModify1=1;
                btnAns1.frame = CGRectMake(21, 20, 175, 173);
                [btnAns1 setImage:[self loadImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans1Img"]] forState:UIControlStateNormal];
            }
            if ([[[strModify componentsSeparatedByString:@","] objectAtIndex:1] isEqualToString:@"0"]) {
                tagModify2=0;
                [btnAns2 setImage:[UIImage imageNamed:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans2Img"]] forState:UIControlStateNormal];
            }else {
                tagModify2=1;
                btnAns2.frame = CGRectMake(21, 20, 175, 173);
                [btnAns2 setImage:[self loadImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans2Img"]] forState:UIControlStateNormal];
            }
        
            if ([[[strModify componentsSeparatedByString:@","] objectAtIndex:2] isEqualToString:@"0"]) {
                tagModify3=0;
                [btnAns3 setImage:[UIImage imageNamed:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans3Img"]] forState:UIControlStateNormal];
            }else {
                tagModify3=1;
                btnAns3.frame = CGRectMake(21, 20, 175, 173);
                [btnAns3 setImage:[self loadImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans3Img"]] forState:UIControlStateNormal];
            }
//            tagModify1=0;
//            tagModify2=0;
//            tagModify3=0;
        }
            break;
        case 2:
        {
            if ([[arrSepAns objectAtIndex:0] isEqualToString:@"E"]) {
                [self setCorrectAns:btnCorAns1];
            }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"F"]) {
                [self setCorrectAns:btnCorAns2];
            }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"G"]) {
                [self setCorrectAns:btnCorAns3];
            }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"H"]) {
                [self setCorrectAns:btnCorAns4];
            }
            if ([[[strModify componentsSeparatedByString:@","] objectAtIndex:0] isEqualToString:@"0"]) {
                tagModify1=0;
                [btnAns1 setImage:[UIImage imageNamed:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans1Img"]] forState:UIControlStateNormal];
            }else {
                tagModify1=1;
                btnAns1.frame = CGRectMake(21, 20, 175, 173);
                [btnAns1 setImage:[self loadImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans1Img"]] forState:UIControlStateNormal];
            }
            if ([[[strModify componentsSeparatedByString:@","] objectAtIndex:1] isEqualToString:@"0"]) {
                tagModify2=0;
                [btnAns2 setImage:[UIImage imageNamed:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans2Img"]] forState:UIControlStateNormal];
            }else {
                tagModify2=1;
                btnAns2.frame = CGRectMake(21, 20, 175, 173);
                [btnAns2 setImage:[self loadImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans2Img"]] forState:UIControlStateNormal];
            }
            if ([[[strModify componentsSeparatedByString:@","] objectAtIndex:2] isEqualToString:@"0"]) {
                tagModify3=0;
                [btnAns3 setImage:[UIImage imageNamed:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans3Img"]] forState:UIControlStateNormal];
            }else {
                tagModify3=1;
                btnAns3.frame = CGRectMake(21, 20, 175, 173);
                [btnAns3 setImage:[self loadImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans3Img"]] forState:UIControlStateNormal];
            }
            if ([[[strModify componentsSeparatedByString:@","] objectAtIndex:3] isEqualToString:@"0"]) {
                tagModify4=0;
                [btnAns4 setImage:[UIImage imageNamed:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans4Img"]] forState:UIControlStateNormal];
            }else {
                tagModify4=1;
                btnAns4.frame = CGRectMake(21, 20, 175, 173);
                [btnAns4 setImage:[self loadImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans4Img"]] forState:UIControlStateNormal];
            }
//            tagModify1=0;
//            tagModify2=0;
//            tagModify3=0;
//            tagModify4=0;
        }
            break;
        case 3:
        {
            if ([[arrSepAns objectAtIndex:0] isEqualToString:@"E"]) {
                [self setCorrectAns:btnCorAns1];
            }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"F"]) {
                [self setCorrectAns:btnCorAns2];
            }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"G"]) {
                [self setCorrectAns:btnCorAns3];
            }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"H"]) {
                [self setCorrectAns:btnCorAns4];
            }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"I"]) {
                [self setCorrectAns:btnCorAns5];
            }  
            if ([[[strModify componentsSeparatedByString:@","] objectAtIndex:0] isEqualToString:@"0"]) {
                tagModify1=0;
                [btnAns1 setImage:[UIImage imageNamed:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans1Img"]] forState:UIControlStateNormal];
            }else {
                tagModify1=1;
                btnAns1.frame = CGRectMake(21, 20, 175, 173);
                [btnAns1 setImage:[self loadImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans1Img"]] forState:UIControlStateNormal];
            }
            if ([[[strModify componentsSeparatedByString:@","] objectAtIndex:1] isEqualToString:@"0"]) {
                tagModify2=0;
                [btnAns2 setImage:[UIImage imageNamed:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans2Img"]] forState:UIControlStateNormal];
            }else {
                tagModify2=1;
                btnAns2.frame = CGRectMake(21, 20, 175, 173);
                [btnAns2 setImage:[self loadImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans2Img"]] forState:UIControlStateNormal];
            }
            if ([[[strModify componentsSeparatedByString:@","] objectAtIndex:2] isEqualToString:@"0"]) {
                tagModify3=0;
                [btnAns3 setImage:[UIImage imageNamed:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans3Img"]] forState:UIControlStateNormal];
            }else {
                tagModify3=1;
                btnAns3.frame = CGRectMake(21, 20, 175, 173);
                [btnAns3 setImage:[self loadImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans3Img"]] forState:UIControlStateNormal];
            }
            if ([[[strModify componentsSeparatedByString:@","] objectAtIndex:3] isEqualToString:@"0"]) {
                tagModify4=0;
                [btnAns4 setImage:[UIImage imageNamed:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans4Img"]] forState:UIControlStateNormal];
            }else {
                tagModify4=1;
                btnAns4.frame = CGRectMake(21, 20, 175, 173);
                [btnAns4 setImage:[self loadImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans4Img"]] forState:UIControlStateNormal];
            }
            if ([[[strModify componentsSeparatedByString:@","] objectAtIndex:4] isEqualToString:@"0"]) {
                tagModify5=0;
                [btnAns5 setImage:[UIImage imageNamed:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans5Img"]] forState:UIControlStateNormal];
            }else {
                tagModify5=1;
                btnAns5.frame = CGRectMake(21, 20, 175, 173);
                [btnAns5 setImage:[self loadImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans5Img"]] forState:UIControlStateNormal];
            }
//            tagModify1=0;
//            tagModify2=0;
//            tagModify3=0;
//            tagModify4=0;
//            tagModify5=0;
        }                
            break;
        default:
            break;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
#pragma mark UIImagePickerController Delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self.popOverOVC dismissPopoverAnimated:YES];
    UIImage *img = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    UIImage *imgCopy = [self rotateImage:img];
    switch (tagOptions) {
        case 1:{
            tagModify1=1;
            btnAns1.frame = CGRectMake(21, 18, 175, 176);
            [btnAns1 setImage:imgCopy forState:UIControlStateNormal];
        }
            break;
        case 2:{
            tagModify2=1;
            btnAns2.frame = CGRectMake(21, 18, 175, 176);
            [btnAns2 setImage:imgCopy forState:UIControlStateNormal];      
        }
            break;
        case 3:{
            tagModify3=1;
            btnAns3.frame = CGRectMake(21, 18, 175, 176);
            [btnAns3 setImage:imgCopy forState:UIControlStateNormal]; 
        }
            break;
        case 4:{
            tagModify4=1;
            btnAns4.frame = CGRectMake(21, 18, 175, 176);
            [btnAns4 setImage:imgCopy forState:UIControlStateNormal];  
        }
            break;
        case 5:{
            tagModify5=1;
            btnAns5.frame = CGRectMake(21, 18, 175, 176);
            [btnAns5 setImage:imgCopy forState:UIControlStateNormal];  
        }
            break;
            
        default:
            break;
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self.popOverOVC dismissPopoverAnimated:YES];
}
#pragma
#pragma mark - UIAlertview delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        if (buttonIndex==0) {
            self.viewSVC = [[SelectSVC alloc] initWithNibName:nil bundle:nil];
            _viewSVC.delegate=self;
            _viewSVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            _viewSVC.modalPresentationStyle = UIModalPresentationFormSheet;
            [_viewSVC getStudID:strStudID];
            [self presentViewController:_viewSVC animated:YES completion:nil];
        }else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    if (alertView.tag == 11) {
        if (buttonIndex == 1) {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                pickerPhoto = [[UIImagePickerController alloc] init];
                pickerPhoto.delegate = self;
                pickerPhoto.sourceType = UIImagePickerControllerSourceTypeCamera;
//                self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:pickerPhoto];

                [self presentViewController:pickerPhoto animated:YES completion:nil];
                pickerPhoto.view.frame = CGRectMake(0.0, 0.0, 1024, 768); //Need to hardcode frame :(

//                self.popOverOVC.popoverContentSize = CGSizeMake(320, 180);
                if (popDir==0) {
                    [self.popOverOVC presentPopoverFromRect:CGRectMake(X-55, Y, 320, 180) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];                    
                }else {
                    [self.popOverOVC presentPopoverFromRect:CGRectMake(X-55, Y+20, 320, 180) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                }
            }else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"your deveice not compatible to take photo" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        }else if (buttonIndex==2){
            pickerPhoto = [[NonRotatingUIImagePickerController alloc] init];
            pickerPhoto.delegate = self;
            if ([UIImagePickerController isSourceTypeAvailable:
                 UIImagePickerControllerSourceTypePhotoLibrary]) 
            {
                pickerPhoto.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
                
            }

            self.popOverOVC  = [[UIPopoverController alloc] initWithContentViewController:pickerPhoto];
            self.popOverOVC.popoverContentSize = CGSizeMake(320, 180);
            if (popDir==0) {
                [self.popOverOVC presentPopoverFromRect:CGRectMake(X-55, Y, 320, 180) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];                    
            }else {
                [self.popOverOVC presentPopoverFromRect:CGRectMake(X-55, Y+20, 320, 180) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            }
        }else if (buttonIndex==3){
            // Create a KKGriddViewController
            ImageCategoryListViewController *categoriesListViewController = [[ImageCategoryListViewController alloc] init];
            categoriesListViewController.delegate = self;
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:categoriesListViewController];
            self.popOverOVC  = [[UIPopoverController alloc] initWithContentViewController:nav];
            self.popOverOVC.popoverContentSize = CGSizeMake(320, 310);
            if (popDir==0) {
                [self.popOverOVC presentPopoverFromRect:CGRectMake(X-55, Y, 320, 310) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];                    
            }else {
                [self.popOverOVC presentPopoverFromRect:CGRectMake(X-55, Y-110, 320, 310) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            }
        }
    }
}



#pragma mark - ImageCategoryViewController Delegate

- (void)imageSelected:(NSString *)imagePath
{
    
    //Do whatever it is that the image capture needs if the imagePath is !nil
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [self choosePreProgrmmedPhoto:imagePath];
    [self.popOverOVC dismissPopoverAnimated:YES];
}

#pragma mark OptionVC delegate methods
-(void)selectStudsDone:(NSString *)idStr
{
    NSLog(@"idStr:%@",idStr);
    if ([idStr isEqualToString:@""]) {
        
    }else {
        NSArray *arrS = [idStr componentsSeparatedByString:@","];
        for (int i=0; i<[arrS count]; i++) {
            [dS setObject:[arrS objectAtIndex:i] forKey:@"StudID"];
            if ([[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT COUNT(id) FROM tblModify where Level='%d' and StudID='%@' and id='%d'",idLevel,[arrS objectAtIndex:i],idQue]] valueForKey:@"Table1"] valueForKey:@"COUNT(id)"] isEqualToString:@"0"]) {
                [objDAL insertRecord:dS inTable:@"tblModify"];
            }else {
                [objDAL updateRecord:dS forID:[NSString stringWithFormat:@"Level='%d' and StudID='%@' and id='%d'",idLevel,[arrS objectAtIndex:i],idQue] inTable:@"tblModify" withValue:@""];
            }            
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Custom methods

//Modify this method to accept the image path or last component of the path
-(void)choosePreProgrmmedPhoto:(NSString *)imagePath
{
  
    NSString *imgName = imagePath;
    
    switch (tagOptions) {
            case 1:{
                tagModify1=2;
                btnAns1.frame = CGRectMake(6, 2, 209, 207);
                [btnAns1 setTitle:imgName forState:UIControlStateNormal];
                [btnAns1 setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            }
                break;
            case 2:{
                tagModify2=2;
                btnAns2.frame = CGRectMake(6, 2, 209, 207);
                [btnAns2 setTitle:imgName forState:UIControlStateNormal];
                [btnAns2 setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];      
            }
                break;
            case 3:{
                tagModify3=2;
                btnAns3.frame = CGRectMake(6, 2, 209, 207);
                [btnAns3 setTitle:imgName forState:UIControlStateNormal];
                [btnAns3 setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal]; 
            }
                break;
            case 4:{
                tagModify4=2;
                btnAns4.frame = CGRectMake(6, 2, 209, 207);
                [btnAns4 setTitle:imgName forState:UIControlStateNormal];
                [btnAns4 setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];  
            }
                break;
            case 5:{
                tagModify5=2;
                btnAns5.frame = CGRectMake(6, 2, 209, 207);
                [btnAns5 setTitle:imgName forState:UIControlStateNormal];
                [btnAns5 setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];  
            }
                break;
                
            default:
                break;
        }
}

-(void)setCorrectAns:(id)sender
{
    if (sender == btnCorAns1) {
        corAns = 1;
        [btnCorAns1 setImage:[UIImage imageNamed:@"checkTrue.png"] forState:UIControlStateNormal];
        [btnCorAns2 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns3 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns4 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns5 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
    }else if (sender == btnCorAns2) {
        corAns = 2;
        [btnCorAns1 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns2 setImage:[UIImage imageNamed:@"checkTrue.png"] forState:UIControlStateNormal];
        [btnCorAns3 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns4 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns5 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
    }else if (sender == btnCorAns3) {
        corAns = 3;
        [btnCorAns1 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns2 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns3 setImage:[UIImage imageNamed:@"checkTrue.png"] forState:UIControlStateNormal];
        [btnCorAns4 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns5 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
    }else if (sender == btnCorAns4) {
        corAns = 4;
        [btnCorAns1 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns2 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns3 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns4 setImage:[UIImage imageNamed:@"checkTrue.png"] forState:UIControlStateNormal];
        [btnCorAns5 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
    }else if (sender == btnCorAns5) {
        corAns = 5;
        [btnCorAns1 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns2 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns3 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns4 setImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btnCorAns5 setImage:[UIImage imageNamed:@"checkTrue.png"] forState:UIControlStateNormal];
    }
}
-(void)loadQueView
{
    switch (idLevel) {
        case 1:
        {
            view1.hidden=NO;
            view2.hidden=NO;
            view3.hidden=NO;
            view1.frame = CGRectMake(161, 187, 220, 254);
            view2.frame = CGRectMake(402, 187, 220, 254);
            view3.frame = CGRectMake(642, 187, 220, 254);
            view4.hidden=YES;
            view5.hidden=YES;        
        }
            break;
        case 2:
        {
            view1.hidden=NO;
            view2.hidden=NO;
            view3.hidden=NO;
            view4.hidden=NO;
            view1.frame = CGRectMake(282, 187, 220, 254);
            view2.frame = CGRectMake(522, 187, 220, 254);
            view3.frame = CGRectMake(282, 450, 220, 254);
            view4.frame = CGRectMake(522, 450, 220, 254);
            view5.hidden=YES;            
        }
            break;
        case 3:
        {
            view1.hidden=NO;
            view2.hidden=NO;
            view3.hidden=NO;
            view4.hidden=NO;
            view5.hidden=NO;
            view1.frame = CGRectMake(161, 187, 220, 254);
            view2.frame = CGRectMake(402, 187, 220, 254);
            view3.frame = CGRectMake(642, 187, 220, 254);
            view4.frame = CGRectMake(282, 450, 220, 254);
            view5.frame = CGRectMake(522, 450, 220, 254);            
        }
            break;
            
        default:
            break;
    }
}
-(UIImage*)loadImage:(NSString *)imgN
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *imgPath = [docsDir stringByAppendingPathComponent:imgN];
    UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgPath]];
    return imgGet;
}
-(void)getID:(int)queID:(int)levelID:(NSString*)idStud
{
    idQue = queID;
    idLevel = levelID;
    strStudID = idStud;
}
-(void)clickHome:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:
     YES];
}
-(void)clickSave:(id)sender
{
    switch (idLevel) {
        case 1:
        {
            if (tagModify1==0 && tagModify2==0 && tagModify3==0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You haven't modify any answer choices" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                return;
            }
            NSString *strModify = [self nameModify];
//            NSLog(@"strModify:%@",strModify);
            if ([[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT COUNT(id) FROM tblModify where Level='1' and StudID='%@' and id='%d'",strStudID,idQue]] valueForKey:@"Table1"] valueForKey:@"COUNT(id)"] isEqualToString:@"0"]) {
                NSString *strAns1=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:0] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns2=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:1] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns3=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:2] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                
                NSString *strM1=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:0] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM2=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:1] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM3=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:2] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                dS = [NSMutableDictionary dictionaryWithObjectsAndKeys:strAns1,@"Ans1Img",strAns2,@"Ans2Img",strAns3,@"Ans3Img",[self getCorrectAns],@"CorrectAns",@"1",@"Level",strStudID,@"StudID",[NSString stringWithFormat:@"%d",idQue],@"id",[NSString stringWithFormat:@"%@,%@,%@",strM1,strM2,strM3],@"Modify",txtQue.text,@"QText", nil];                
                //NSMutableDictionary *d = [NSMutableDictionary dictionaryWithObjectsAndKeys:strAns1,@"Ans1Img",strAns2,@"Ans2Img",strAns3,@"Ans3Img",[self getCorrectAns],@"CorrectAns",@"1",@"Level",strStudID,@"StudID",[NSString stringWithFormat:@"%d",idQue],@"id",[NSString stringWithFormat:@"%@,%@,%@",strM1,strM2,strM3],@"Modify",txtQue.text,@"QText", nil];
                [objDAL insertRecord:dS inTable:@"tblModify"];
            }else {
                
                NSString *strAns1=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:0] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns2=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:1] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns3=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:2] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                
                NSString *strM1=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:0] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM2=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:1] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM3=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:2] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                dS = [NSMutableDictionary dictionaryWithObjectsAndKeys:strAns1,@"Ans1Img",strAns2,@"Ans2Img",strAns3,@"Ans3Img",[self getCorrectAns],@"CorrectAns",@"1",@"Level",strStudID,@"StudID",[NSString stringWithFormat:@"%d",idQue],@"id",[NSString stringWithFormat:@"%@,%@,%@",strM1,strM2,strM3],@"Modify",txtQue.text,@"QText", nil];                
                //NSMutableDictionary *d = [NSMutableDictionary dictionaryWithObjectsAndKeys:strAns1,@"Ans1Img",strAns2,@"Ans2Img",strAns3,@"Ans3Img",[self getCorrectAns],@"CorrectAns",[NSString stringWithFormat:@"%@,%@,%@",strM1,strM2,strM3],@"Modify", nil];
                [objDAL updateRecord:dS forID:[NSString stringWithFormat:@"Level='1' and StudID='%@' and id='%d'",strStudID,idQue] inTable:@"tblModify" withValue:@""];
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your changes were saved" message:@"Save in another student’s settings?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
            [alert show];
            alert.tag = 1;
        }
            break;
        case 2:
        {
            if (tagModify1==0 && tagModify2==0 && tagModify3==0 && tagModify4==0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You haven't modify any answer choices" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                return; 
            }           
            NSString *strModify = [self nameModify];
//            NSLog(@"strModify:%@",strModify);
            if ([[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT COUNT(id) FROM tblModify where Level='2' and StudID='%@' and id='%d'",strStudID,idQue]] valueForKey:@"Table1"] valueForKey:@"COUNT(id)"] isEqualToString:@"0"]) {
                NSString *strAns1=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:0] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns2=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:1] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns3=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:2] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns4=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:3] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                
                NSString *strM1=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:0] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM2=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:1] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM3=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:2] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM4=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:3] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                dS = [NSMutableDictionary dictionaryWithObjectsAndKeys:strAns1,@"Ans1Img",strAns2,@"Ans2Img",strAns3,@"Ans3Img",strAns4,@"Ans4Img",[self getCorrectAns],@"CorrectAns",@"2",@"Level",strStudID,@"StudID",[NSString stringWithFormat:@"%d",idQue],@"id",[NSString stringWithFormat:@"%@,%@,%@,%@",strM1,strM2,strM3,strM4],@"Modify",txtQue.text,@"QText", nil];                
                //NSMutableDictionary *d = [NSMutableDictionary dictionaryWithObjectsAndKeys:strAns1,@"Ans1Img",strAns2,@"Ans2Img",strAns3,@"Ans3Img",strAns4,@"Ans4Img",[self getCorrectAns],@"CorrectAns",@"2",@"Level",strStudID,@"StudID",[NSString stringWithFormat:@"%d",idQue],@"id",[NSString stringWithFormat:@"%@,%@,%@,%@",strM1,strM2,strM3,strM4],@"Modify",txtQue.text,@"QText", nil];
                [objDAL insertRecord:dS inTable:@"tblModify"];
            }else {
                
                NSString *strAns1=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:0] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns2=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:1] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns3=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:2] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns4=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:3] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                
                NSString *strM1=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:0] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM2=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:1] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM3=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:2] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM4=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:3] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                dS = [NSMutableDictionary dictionaryWithObjectsAndKeys:strAns1,@"Ans1Img",strAns2,@"Ans2Img",strAns3,@"Ans3Img",strAns4,@"Ans4Img",[self getCorrectAns],@"CorrectAns",@"2",@"Level",strStudID,@"StudID",[NSString stringWithFormat:@"%d",idQue],@"id",[NSString stringWithFormat:@"%@,%@,%@,%@",strM1,strM2,strM3,strM4],@"Modify",txtQue.text,@"QText", nil];
//                NSMutableDictionary *d = [NSMutableDictionary dictionaryWithObjectsAndKeys:strAns1,@"Ans1Img",strAns2,@"Ans2Img",strAns3,@"Ans3Img",strAns4,@"Ans4Img",[self getCorrectAns],@"CorrectAns",[NSString stringWithFormat:@"%@,%@,%@,%@",strM1,strM2,strM3,strM4],@"Modify", nil];
                [objDAL updateRecord:dS forID:[NSString stringWithFormat:@"Level='2' and StudID='%@' and id='%d'",strStudID,idQue] inTable:@"tblModify" withValue:@""];
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your changes were saved" message:@"Save in another student’s settings?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
            [alert show];
            alert.tag = 1;
        }
            break;
        case 3:
        {
            if (tagModify1==0 && tagModify2==0 && tagModify3==0 && tagModify4==0 && tagModify5==0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You haven't modify any answer choices" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                return;                
            } 
            NSString *strModify = [self nameModify];
//            NSLog(@"strModify:%@",strModify);
            if ([[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT COUNT(id) FROM tblModify where Level='3' and StudID='%@' and id='%d'",strStudID,idQue]] valueForKey:@"Table1"] valueForKey:@"COUNT(id)"] isEqualToString:@"0"]) {
                NSString *strAns1=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:0] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns2=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:1] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns3=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:2] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns4=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:3] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns5=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:4] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                
                NSString *strM1=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:0] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM2=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:1] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM3=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:2] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM4=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:3] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM5=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:4] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                dS = [NSMutableDictionary dictionaryWithObjectsAndKeys:strAns1,@"Ans1Img",strAns2,@"Ans2Img",strAns3,@"Ans3Img",strAns4,@"Ans4Img",strAns5,@"Ans5Img",[self getCorrectAns],@"CorrectAns",@"3",@"Level",strStudID,@"StudID",[NSString stringWithFormat:@"%d",idQue],@"id",[NSString stringWithFormat:@"%@,%@,%@,%@,%@",strM1,strM2,strM3,strM4,strM5],@"Modify",txtQue.text,@"QText", nil];
                //NSMutableDictionary *d = [NSMutableDictionary dictionaryWithObjectsAndKeys:strAns1,@"Ans1Img",strAns2,@"Ans2Img",strAns3,@"Ans3Img",strAns4,@"Ans4Img",strAns5,@"Ans5Img",[self getCorrectAns],@"CorrectAns",@"3",@"Level",strStudID,@"StudID",[NSString stringWithFormat:@"%d",idQue],@"id",[NSString stringWithFormat:@"%@,%@,%@,%@,%@",strM1,strM2,strM3,strM4,strM5],@"Modify",txtQue.text,@"QText", nil];
                [objDAL insertRecord:dS inTable:@"tblModify"];
            }else {
                
                NSString *strAns1=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:0] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns2=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:1] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns3=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:2] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns4=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:3] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                NSString *strAns5=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:4] componentsSeparatedByString:@"+++"] objectAtIndex:0];
                
                NSString *strM1=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:0] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM2=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:1] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM3=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:2] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM4=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:3] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                NSString *strM5=[[[[strModify componentsSeparatedByString:@"***"] objectAtIndex:4] componentsSeparatedByString:@"+++"] objectAtIndex:1];
                dS = [NSMutableDictionary dictionaryWithObjectsAndKeys:strAns1,@"Ans1Img",strAns2,@"Ans2Img",strAns3,@"Ans3Img",strAns4,@"Ans4Img",strAns5,@"Ans5Img",[self getCorrectAns],@"CorrectAns",@"3",@"Level",strStudID,@"StudID",[NSString stringWithFormat:@"%d",idQue],@"id",[NSString stringWithFormat:@"%@,%@,%@,%@,%@",strM1,strM2,strM3,strM4,strM5],@"Modify",txtQue.text,@"QText", nil];
                //NSMutableDictionary *d = [NSMutableDictionary dictionaryWithObjectsAndKeys:strAns1,@"Ans1Img",strAns2,@"Ans2Img",strAns3,@"Ans3Img",strAns4,@"Ans4Img",strAns5,@"Ans5Img",[self getCorrectAns],@"CorrectAns",[NSString stringWithFormat:@"%@,%@,%@,%@,%@",strM1,strM2,strM3,strM4,strM5],@"Modify", nil];
                [objDAL updateRecord:dS forID:[NSString stringWithFormat:@"Level='3' and StudID='%@' and id='%d'",strStudID,idQue] inTable:@"tblModify" withValue:@""];
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your changes were saved" message:@"Save in another student’s settings?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
            [alert show];
            alert.tag = 1;
        }                
            break;
        default:
            break;
    }
}
-(NSString*)nameModify{
    NSMutableArray *arr = [NSMutableArray array];
    switch (idLevel) {
        case 1:
        {
            [arr addObject:[self namePhoto:1]];
            [arr addObject:[self namePhoto:2]];
            [arr addObject:[self namePhoto:3]];
        }
            break;
        case 2:
        {
            [arr addObject:[self namePhoto:1]];
            [arr addObject:[self namePhoto:2]];
            [arr addObject:[self namePhoto:3]];
            [arr addObject:[self namePhoto:4]];
        }
            break;
        case 3:
        {
            [arr addObject:[self namePhoto:1]];
            [arr addObject:[self namePhoto:2]];
            [arr addObject:[self namePhoto:3]]; 
            [arr addObject:[self namePhoto:4]];
            [arr addObject:[self namePhoto:5]];
        }                
            break;
        default:
            break;
    }
    NSString *str = [arr componentsJoinedByString:@"***"];
    return str;
}
-(NSString*)namePhoto:(int)img{
    NSMutableArray *arr = [NSMutableArray array];
    switch (img) {
        case 1:
        {
            if (tagModify1==0) {
                [arr addObject:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans1Img"]];
                [arr addObject:@"0"];
            }else if (tagModify1==1) {
                if ([[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] isEqualToString:@""]) {
                    [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns1 imageForState:UIControlStateNormal]):1:@""]];
                }else {
                    if ([[[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","] objectAtIndex:img-1] isEqualToString:@"1"]) {
                        [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns1 imageForState:UIControlStateNormal]):1:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans1Img"]]];
                    }else {
                        [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns1 imageForState:UIControlStateNormal]):1:@""]];
                    }
                }
                [arr addObject:@"1"];
            }else if (tagModify1==2) {
                if ([[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] isEqualToString:@""]) {
                }else {
                    if ([[[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","] objectAtIndex:img-1] isEqualToString:@"1"]) {
                        if ([self removeImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans1Img"]]) {
                            NSMutableArray *arrM = [NSMutableArray arrayWithArray:[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","]];
                            [arrM replaceObjectAtIndex:img-1 withObject:@"0"];
                            [[dictQue valueForKey:@"Table1"] setObject:[arrM componentsJoinedByString:@","] forKey:@"Modify"];
                        }
                    }
                }
                [arr addObject:btnAns1.titleLabel.text];
                [arr addObject:@"0"];
            }        
        }
            break;
        case 2:
        {
            if (tagModify2==0) {
                [arr addObject:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans2Img"]];
                [arr addObject:@"0"];
            }else if (tagModify2==1) {
                if ([[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] isEqualToString:@""]) {
                    [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns2 imageForState:UIControlStateNormal]):2:@""]];
                }else {
                    if ([[[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","] objectAtIndex:img-1] isEqualToString:@"1"]) {
                        [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns2 imageForState:UIControlStateNormal]):2:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans2Img"]]];
                    }else {
                        [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns2 imageForState:UIControlStateNormal]):2:@""]];
                    }
                }
                [arr addObject:@"1"];
            }else if (tagModify2==2) {
                if ([[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] isEqualToString:@""]) {
                }else {
                    if ([[[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","] objectAtIndex:img-1] isEqualToString:@"1"]) {
                        if ([self removeImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans2Img"]]) {
                            NSMutableArray *arrM = [NSMutableArray arrayWithArray:[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","]];
                            [arrM replaceObjectAtIndex:img-1 withObject:@"0"];
                            [[dictQue valueForKey:@"Table1"] setObject:[arrM componentsJoinedByString:@","] forKey:@"Modify"];
                        }
                    }
                }
                [arr addObject:btnAns2.titleLabel.text];
                [arr addObject:@"0"];
            }            
        }
            break;
        case 3:
        {
            if (tagModify3==0) {
                [arr addObject:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans3Img"]];
                [arr addObject:@"0"];
            }else if (tagModify3==1) {
                if ([[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] isEqualToString:@""]) {
                    [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns3 imageForState:UIControlStateNormal]):3:@""]];
                }else {
                    if ([[[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","] objectAtIndex:img-1] isEqualToString:@"1"]) {
                        [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns3 imageForState:UIControlStateNormal]):3:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans3Img"]]];
                    }else {
                        [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns3 imageForState:UIControlStateNormal]):3:@""]];
                    }
                }
                [arr addObject:@"1"];
            }else if (tagModify3==2) {
                if ([[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] isEqualToString:@""]) {
                }else {
                    if ([[[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","] objectAtIndex:img-1] isEqualToString:@"1"]) {
                        if ([self removeImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans3Img"]]) {
                            NSMutableArray *arrM = [NSMutableArray arrayWithArray:[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","]];
                            [arrM replaceObjectAtIndex:img-1 withObject:@"0"];
                            [[dictQue valueForKey:@"Table1"] setObject:[arrM componentsJoinedByString:@","] forKey:@"Modify"];
                        }
                    }
                }
                [arr addObject:btnAns3.titleLabel.text];
                [arr addObject:@"0"];
            }            
        }
            break;
        case 4:
        {
            if (tagModify4==0) {
                [arr addObject:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans4Img"]];
                [arr addObject:@"0"];
            }else if (tagModify4==1) {
                if ([[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] isEqualToString:@""]) {
                    [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns4 imageForState:UIControlStateNormal]):4:@""]];
                }else {
                    if ([[[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","] objectAtIndex:img-1] isEqualToString:@"1"]) {
                        [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns4 imageForState:UIControlStateNormal]):4:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans4Img"]]];
                    }else {
                        [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns4 imageForState:UIControlStateNormal]):4:@""]];
                    }
                }
                [arr addObject:@"1"];
            }else if (tagModify4==2) {
                if ([[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] isEqualToString:@""]) {
                }else {
                    if ([[[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","] objectAtIndex:img-1] isEqualToString:@"1"]) {
                        if ([self removeImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans4Img"]]) {
                            NSMutableArray *arrM = [NSMutableArray arrayWithArray:[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","]];
                            [arrM replaceObjectAtIndex:img-1 withObject:@"0"];
                            [[dictQue valueForKey:@"Table1"] setObject:[arrM componentsJoinedByString:@","] forKey:@"Modify"];
                        }
                    }
                }
                [arr addObject:btnAns4.titleLabel.text];
                [arr addObject:@"0"];
            }            
        }
            break;
        case 5:
        {
            if (tagModify5==0) {
                [arr addObject:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans5Img"]];
                [arr addObject:@"0"];
            }else if (tagModify5==1) {
                if ([[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] isEqualToString:@""]) {
                    [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns5 imageForState:UIControlStateNormal]):5:@""]];
                }else {
                    if ([[[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","] objectAtIndex:img-1] isEqualToString:@"1"]) {
                        [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns5 imageForState:UIControlStateNormal]):5:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans5Img"]]];   
                    }else {
                        [arr addObject:[self clickSaveImage:UIImagePNGRepresentation([btnAns5 imageForState:UIControlStateNormal]):5:@""]];
                    }
                }
                [arr addObject:@"1"];
            }else if (tagModify5==2) {
                if ([[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] isEqualToString:@""]) {
                }else {
                    if ([[[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","] objectAtIndex:img-1] isEqualToString:@"1"]) {
                        if ([self removeImage:[[dictQue valueForKey:@"Table1"] valueForKey:@"Ans5Img"]]) {
                            NSMutableArray *arrM = [NSMutableArray arrayWithArray:[[[dictQue valueForKey:@"Table1"] valueForKey:@"Modify"] componentsSeparatedByString:@","]];
                            [arrM replaceObjectAtIndex:img-1 withObject:@"0"];
                            [[dictQue valueForKey:@"Table1"] setObject:[arrM componentsJoinedByString:@","] forKey:@"Modify"];
                        }
                    }
                }
                [arr addObject:btnAns5.titleLabel.text];
                [arr addObject:@"0"];
            }            
        }
            break;
            
        default:
            break;
    }
    NSString *strPN = [arr componentsJoinedByString:@"+++"];
    return strPN;
}
-(BOOL)removeImage:(NSString*)strImg 
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *docsDir = [dirPaths objectAtIndex:0];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *imgSImg = [docsDir stringByAppendingPathComponent:strImg];
	UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgSImg]];		
	if (imgGet==NULL) {
        return NO;
	}
    [fileManager removeItemAtPath:imgSImg error:NULL];		
    return YES;
}
-(NSString*)clickSaveImage:(NSData*)dataBtnImg:(int)btn:(NSString*)nameStr
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    if ([nameStr isEqualToString:@""]) {
        NSString *strImgN = [NSString stringWithFormat:@"%@%d.png",[NSDate date],btn];
        NSString *imgPath = [docsDir stringByAppendingPathComponent:strImgN];
        [[NSFileManager defaultManager] createFileAtPath:imgPath contents:dataBtnImg attributes:nil];
        return strImgN;        
    }
    NSString *imgPath = [docsDir stringByAppendingPathComponent:nameStr];
    [[NSFileManager defaultManager] createFileAtPath:imgPath contents:dataBtnImg attributes:nil];
    return nameStr;
}
-(NSString*)getCorrectAns
{
    NSString *strCorA=@"";
    switch (idLevel) {
        case 1:
        {
            switch (corAns) {
                case 1:
                    strCorA = @"E & J";
                    break;
                case 2:
                    strCorA = @"F & K";
                    break;
                case 3:
                    strCorA = @"G & L";
                    break;
                default:
                    break;
            }
        }
            break;
        case 2:
        {
            switch (corAns) {
                case 1:
                    strCorA = @"E & K";
                    break;
                case 2:
                    strCorA = @"F & L";
                    break;
                case 3:
                    strCorA = @"G & M";
                    break;
                case 4:
                    strCorA = @"H & N";
                    break;
                default:
                    break;
            }
        }
            break;
        case 3:
        {
            switch (corAns) {
                case 1:
                    strCorA = @"E & L";
                    break;
                case 2:
                    strCorA = @"F & M";
                    break;
                case 3:
                    strCorA = @"G & N";
                    break;
                case 4:
                    strCorA = @"H & O";
                    break;
                case 5:
                    strCorA = @"I & P";
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    return strCorA;
}
-(void)clickSelectOptions:(id)sender
{
    if (sender == btnAns1) {
        tagOptions = 1;
        if (idLevel==2) {
            X = 282;
            Y = 187;
            popDir=1;
        }else {
            X = 161;
            Y = 187; 
            popDir=1;
        }
    }else if (sender == btnAns2) {
        tagOptions = 2;
        if (idLevel==2) {
            X = 522;
            Y = 187;
            popDir=1;
        }else {
            X = 402;
            Y = 187; 
            popDir=1;
        }
    }else if (sender == btnAns3) {
        tagOptions = 3;
        if (idLevel==2) {
            X = 282;
            Y = 450;
            popDir =0;
        }else {
            X = 642;
            Y = 187; 
            popDir=1;
        }
    }else if (sender == btnAns4) {
        tagOptions = 4;
        if (idLevel==2) {
            X = 522;
            Y = 450;
            popDir=0;
        }else {
            X = 282;
            Y = 450;
            popDir=0;
        }
    }else if (sender == btnAns5) {
        tagOptions = 5;
        X = 522;
        Y = 450;
        popDir=0;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Take photo" message:@"From" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Photos library",@"Preprogrammed pictures", nil];
    alert.tag = 11;
    [alert show];
}
-(UIImage *)rotateImage:(UIImage *)image {
    
    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
} 
@end
