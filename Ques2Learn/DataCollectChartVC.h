//
//  DataCollectChartVC.h
//  Ques2Learn
//
//  Created by apple on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionsVC.h"
#import "AddStudentVC.h"
#import "OptionVC1.h"
@interface DataCollectChartVC : UIViewController <UIPopoverControllerDelegate,OptionsVCDelegate,AddStudentVCDelegate,OptionsVC1Delegate>{
    OptionsVC *viewOptionsVC;
    OptionVC1 *viewOptionsVC1;
    AddStudentVC *viewAddStud;
    IBOutlet UIImageView *imgSwtNT;
    IBOutlet UIButton *btnSImg;
    IBOutlet UIButton *btnAddS;
    UIPopoverController *popOverOVC;
    int btnTag;
    NSMutableArray *arrStu_List;
    NSMutableArray *arrGroup;
    NSMutableArray *arrCollect;
    IBOutlet UITableView *tblStudents;
    IBOutlet UILabel *lblStudName;
    IBOutlet UILabel *lblStudAge;
    IBOutlet UILabel *lblStudGrade;
    IBOutlet UILabel *lblNOT;
    DAL *objDAL;
    NSString *strStuID;
    NSIndexPath *tagR;
    
    IBOutlet UIButton *btnEdit;
    IBOutlet UILabel *lblTAge;
    IBOutlet UILabel *lblTGrade;
}
@property (nonatomic, retain) OptionsVC *viewOptionsVC;
@property (nonatomic, retain) OptionVC1 *viewOptionsVC1;
@property (nonatomic, retain) AddStudentVC *viewAddStud;
@property (nonatomic, retain) UIPopoverController *popOverOVC;
-(IBAction)clickHome:(id)sender;
-(void)loadGroupSettings;
-(IBAction)clickSetOptions:(id)sender;
-(IBAction)clickSwitchOpt:(id)sender;
-(IBAction)clickCreateNewStudent:(id)sender;
-(IBAction)clickEdit:(id)sender;
-(IBAction)clickDC:(id)sender;
-(IBAction)clickCustomQue:(id)sender;
-(IBAction)clickGo:(id)sender;
-(NSMutableArray *)getSudentsRecords;
-(void)clickStudRecord:(NSInteger)tagRow;
-(void)loadBlankView;
-(void)clickSetAddBtn;
@end
