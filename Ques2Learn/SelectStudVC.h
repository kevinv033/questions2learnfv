//
//  SelectStudVC.h
//  Ques2Learn
//
//  Created by apple on 4/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAL.h"
@protocol SelectStudVCDelegate
- (void)selectStudsDone:(NSInteger)idStr;
@end
@interface SelectStudVC : UIViewController {
    IBOutlet UITableView *tblS;
    NSMutableArray *arrStud;
    DAL *objDAL;
    NSInteger strID;
    id<SelectStudVCDelegate> delegate;
}
@property (nonatomic, retain) id<SelectStudVCDelegate> delegate;
-(IBAction)clickDone:(id)sender;
@end
