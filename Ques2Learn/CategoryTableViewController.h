//
//  CategoryTableViewController.h
//  Ques2Learn
//
//  Created by Kevin Vertucio on 07/12/13.
//
//  This class will manage the tableview for selecting the Category for questions to display within the Modify Preprogrammed Question UI

#import <UIKit/UIKit.h>

@protocol PopOverSelectionDelegate

@property (nonatomic,weak) id <PopOverSelectionDelegate> popoverDelegate;

@optional

- (void)popOverItemSelected:(NSString *)selectedItem;

@end

@interface CategoryTableViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>
{
    id <PopOverSelectionDelegate> popoverDelegate;
}

@property (strong) id popoverDelegate;

@property (strong, nonatomic) NSString *questionCategory;

@property (strong, nonatomic) NSMutableArray *categoryArray;

@end
