//
//  ModifyPreProgVC.h
//  Ques2Learn
//
//  Created by apple on 5/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "CategoryTableViewController.h"


@interface ModifyPreProgVC : UIViewController <UITableViewDelegate,UITableViewDataSource,UIPopoverControllerDelegate,PopOverSelectionDelegate>{
    int tagLevel;
    NSMutableArray *allQue;
    NSMutableArray *arrQue;
    NSMutableArray *allCat;
    NSMutableArray *arrCat;
// Arrays for categorized questions
    NSMutableArray *schoolQue;
    NSMutableArray *homeQue;
    NSMutableArray *foodQue;
    NSMutableArray *healthQue;
    NSMutableArray *communityQue;
    NSMutableArray *leisureQue;    
    DAL *objDAL;
    int idQue;
    NSString *strStudID;
    IBOutlet UITableView *tbl;
    UIPopoverController *popover;
    CategoryTableViewController *catView;
}
-(void)getStudID:(NSString*)idStr;
-(IBAction)clickHome:(id)sender;
-(IBAction)clickBack:(id)sender;
-(IBAction)clickSelectCategory:(id)sender;
-(void)reloadTable;
-(void)reloadTableWithCategory:(NSString *)category;
-(IBAction)clickChooseLevel:(id)sender;
-(IBAction)clickModify:(id)sender;
-(IBAction)openDirections:(id)sender;
@end
