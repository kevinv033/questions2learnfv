//
//  SelectRecordVC.h
//  Ques2Learn
//
//  Created by apple on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SelectRVCDelegate
- (void)selectRecordsDone:(NSMutableArray*)arrIndStr;
@end

@interface SelectRecordVC : UIViewController {
    id <SelectRVCDelegate> delegate;
    NSMutableArray *arrR;
    NSMutableArray *arrGroup;
    NSMutableArray *arrCollect;
}
@property (nonatomic,retain) id <SelectRVCDelegate> delegate;

-(void)getRecords:(NSMutableArray*)recArr;
-(IBAction)clickDone:(id)sender;
@end
