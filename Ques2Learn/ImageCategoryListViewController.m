//
//  ImageCategoryListViewController.m
//  Ques2Learn
//
//  Created by Kevin Vertucio on 09/9/13.
//
//

#import "ImageCategoryListViewController.h"
#import "ImageGridViewController.h"
#import <JSONKit/JSONKit.h>


@interface ImageCategoryListViewController ()
{
    NSMutableDictionary *resultsDictionary;
    NSMutableDictionary *imageDictionary;
    NSMutableArray *imgCategoriesArray;
}
@end

@implementation ImageCategoryListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = @"Choose a category";
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    //Parse JSON data
    NSString *jsonFile = [[NSBundle mainBundle] pathForResource:@"Q2L-categories-images" ofType:@"json"];
    
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"Path to JSON: %@", jsonFile);
#endif
    
    //Create Dictionary of items
    NSData *jsonData = [NSData dataWithContentsOfFile:jsonFile];
    
    resultsDictionary = [jsonData objectFromJSONData];
    
    //Create model of categories
    imgCategoriesArray = [NSMutableArray arrayWithCapacity:0];
    for (NSDictionary *item in resultsDictionary) {
        NSString *category = [item objectForKey:@"Category"];
        if (![imgCategoriesArray containsObject:category])
            [imgCategoriesArray addObject:category];
    }    
    
    //Create model of images
    imageDictionary = [NSMutableDictionary dictionaryWithCapacity:0];
    for (NSString *category in imgCategoriesArray) {
        NSMutableArray *imgArray = [NSMutableArray arrayWithCapacity:0];
        for (NSDictionary *item in resultsDictionary) {
            if ([category isEqualToString:[item objectForKey:@"Category"]]) {
                [imgArray addObject:[item objectForKey:@"Image"]];
            }
        }
        [imageDictionary setObject:imgArray forKey:category];
    }
    
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"%@", imageDictionary);
#endif

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [imgCategoriesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    cell.textLabel.text = [[imgCategoriesArray objectAtIndex:indexPath.row] capitalizedString];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
     ImageGridViewController *detailViewController = [[ImageGridViewController alloc] init];
    detailViewController.delegate = self.navigationController;
    detailViewController.category = [imgCategoriesArray objectAtIndex:indexPath.row];
    detailViewController.imageArray = [imageDictionary objectForKey:[imgCategoriesArray objectAtIndex:indexPath.row]];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
    
}

#pragma mark - ImageGridViewControllerDelegate Methods

-(void)imageSelected:(NSString *)image
{
    //Send this string out to the main vc
    [self.delegate imageSelected:image];
    
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"Image Selected: %@", image);
#endif
    
}


@end
