//
//  SelectRecordVC.m
//  Ques2Learn
//
//  Created by apple on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SelectRecordVC.h"

@implementation SelectRecordVC
@synthesize delegate = _delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
#pragma
#pragma mark - UITableView Delegate mathods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrR count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
    UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 79, 44)];
    lbl1.text = [[arrR objectAtIndex:indexPath.row] valueForKey:@"Date"];
    lbl1.textAlignment = UITextAlignmentCenter;
    lbl1.font = [UIFont fontWithName:@"Helvetica" size:12];
    
    UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(79, 0, 45, 44)];
    lbl2.text = [[arrR objectAtIndex:indexPath.row] valueForKey:@"Level"];
    lbl2.textAlignment = UITextAlignmentCenter;
    lbl2.font = [UIFont fontWithName:@"Helvetica" size:12];
    UILabel *lbl3 = [[UILabel alloc] initWithFrame:CGRectMake(124, 0, 89, 44)];
    NSString *strCat = [[arrR objectAtIndex:indexPath.row] valueForKey:@"Category"];
    lbl3.text = strCat;
    lbl3.textAlignment = UITextAlignmentCenter;
    lbl3.font = [UIFont fontWithName:@"Helvetica" size:12];
    lbl3.numberOfLines = 2;
    
    UILabel *lbl4 = [[UILabel alloc] initWithFrame:CGRectMake(213, 0, 58, 44)];
    double in1 = [[[arrR objectAtIndex:indexPath.row] valueForKey:@"ScoreWho"] doubleValue];
    double cn1 = [[[arrR objectAtIndex:indexPath.row] valueForKey:@"CntWho"] doubleValue];
    if (cn1 == 0) {
        lbl4.text = [NSString stringWithFormat:@"0 %@",@"%"];
    }else {
        lbl4.text = [NSString stringWithFormat:@"%.f %@",(in1/cn1)*100,@"%"];
    }
    lbl4.textAlignment = UITextAlignmentCenter;
    lbl4.font = [UIFont fontWithName:@"Helvetica" size:12];
    
    UILabel *lbl5 = [[UILabel alloc] initWithFrame:CGRectMake(271, 0, 58, 44)];
    double in2 = [[[arrR objectAtIndex:indexPath.row] valueForKey:@"ScoreWhat"] intValue];
    double cn2 = [[[arrR objectAtIndex:indexPath.row] valueForKey:@"CntWhat"] intValue];
    if (cn2 == 0) {
        lbl5.text = [NSString stringWithFormat:@"0 %@",@"%"];
    }else {
        lbl5.text = [NSString stringWithFormat:@"%.f %@",(in2/cn2)*100,@"%"];
    }
    lbl5.textAlignment = UITextAlignmentCenter;
    lbl5.font = [UIFont fontWithName:@"Helvetica" size:12];
    
    UILabel *lbl6 = [[UILabel alloc] initWithFrame:CGRectMake(329, 0, 58, 44)];
    double in3 = [[[arrR objectAtIndex:indexPath.row] valueForKey:@"ScoreWhen"] intValue];
    double cn3 = [[[arrR objectAtIndex:indexPath.row] valueForKey:@"CntWhen"] intValue];
    if (cn3 == 0) {
        lbl6.text = [NSString stringWithFormat:@"0 %@",@"%"];
    }else {
        lbl6.text = [NSString stringWithFormat:@"%.f %@",(in3/cn3)*100,@"%"];
    }
    lbl6.textAlignment = UITextAlignmentCenter;
    lbl6.font = [UIFont fontWithName:@"Helvetica" size:12];
    
    UILabel *lbl7 = [[UILabel alloc] initWithFrame:CGRectMake(387, 0, 58, 44)];
    double in4 = [[[arrR objectAtIndex:indexPath.row] valueForKey:@"ScoreWhere"] intValue];
    double cn4 = [[[arrR objectAtIndex:indexPath.row] valueForKey:@"CntWhere"] intValue];
    if (cn4 == 0) {
        lbl7.text = [NSString stringWithFormat:@"0 %@",@"%"];
    }else {
        lbl7.text = [NSString stringWithFormat:@"%.f %@",(in4/cn4)*100,@"%"];
    }
    lbl7.textAlignment = UITextAlignmentCenter;
    lbl7.font = [UIFont fontWithName:@"Helvetica" size:12];
    
    UILabel *lbl8 = [[UILabel alloc] initWithFrame:CGRectMake(445, 0, 90, 44)];
    if ([[[arrR objectAtIndex:indexPath.row] valueForKey:@"VPrompt"] isEqualToString:@"1"]) {
        lbl8.text = @"ON";
    }else {
        lbl8.text = @"OFF";
    }
    lbl8.textAlignment = UITextAlignmentCenter;
    lbl8.font = [UIFont fontWithName:@"Helvetica" size:12];
    
    [cell.contentView addSubview:lbl1];
    [cell.contentView addSubview:lbl2];
    [cell.contentView addSubview:lbl3];
    [cell.contentView addSubview:lbl4];
    [cell.contentView addSubview:lbl5];
    [cell.contentView addSubview:lbl6];
    [cell.contentView addSubview:lbl7];
    [cell.contentView addSubview:lbl8];
    if ([[arrCollect objectAtIndex:indexPath.row] isEqualToString:@"YES"]) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([[tableView cellForRowAtIndexPath:indexPath] accessoryType] == UITableViewCellAccessoryCheckmark){
        [arrGroup removeObject:[arrR objectAtIndex:indexPath.row]];
        [arrCollect replaceObjectAtIndex:indexPath.row withObject:@"NO"];
    }
    else {
//        [arrGroup removeAllObjects];
//        for (int i=0; i<[arrCollect count]; i++) {
//            [arrCollect replaceObjectAtIndex:i withObject:@"NO"];
//            
//        }
        [arrGroup addObject:[arrR objectAtIndex:indexPath.row]];
        [arrCollect replaceObjectAtIndex:indexPath.row withObject:@"YES"];
    }
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
}
#pragma mark - custom
-(void)getRecords:(NSMutableArray*)recArr
{
    arrGroup = [[NSMutableArray alloc] init];
    arrCollect = [[NSMutableArray alloc] init];
    arrR = [[NSMutableArray alloc] init];
    [arrR setArray:recArr];
    for (int i=0; i<[arrR count]; i++) {
        [arrCollect addObject:@"NO"];
    }
}
-(IBAction)clickDone:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
    [_delegate selectRecordsDone:arrCollect];
}
@end
