//
//  ImageSelectViewController.m
//  Ques2Learn
//
//  Created by Kevin Vertucio on 09/6/13.
//
//

#import "ImageSelectViewController.h"
#import <KKGridView/KKGridView.h>
#import <KKGridView/KKGridViewCell.h>
#import <KKGridView/KKIndexPath.h>
#import <JSONKit/JSONKit.h>

/*Class Macros*/
#define kHeaderHeight 23.0f


@interface ImageSelectViewController ()
{
    NSMutableArray *imgCategoriesArray;     //Array of categories
    NSDictionary *resultsDictionary;        //Dictionary from JSON data
    NSMutableDictionary *imageDictionary;    //Dictionary of filtered images
}
@end

@implementation ImageSelectViewController

- (void)loadView
{
    [super loadView];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.gridView.cellSize = CGSizeMake(95.0f,95.0f);
    
    //Parse JSON data
    NSString *jsonFile = [[NSBundle mainBundle] pathForResource:@"Q2L-categories-images" ofType:@"json"];
    
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"Path to JSON: %@", jsonFile);
#endif
    
    //Create Dictionary of items
    NSData *jsonData = [NSData dataWithContentsOfFile:jsonFile];

    resultsDictionary = [jsonData objectFromJSONData];
    
    //Create model of categories
    imgCategoriesArray = [NSMutableArray arrayWithCapacity:0];
    for (NSDictionary *item in resultsDictionary) {
        NSString *category = [item objectForKey:@"Category"];
        if (![imgCategoriesArray containsObject:category])
            [imgCategoriesArray addObject:category];
    }
    
    [self.gridView reloadData];

    
    //Create model of images
    imageDictionary = [NSMutableDictionary dictionaryWithCapacity:0];
    for (NSString *category in imgCategoriesArray) {
        NSMutableArray *imgArray = [NSMutableArray arrayWithCapacity:0];
        for (NSDictionary *item in resultsDictionary) {
            if ([category isEqualToString:[item objectForKey:@"Category"]]) {
                [imgArray addObject:[item objectForKey:@"Image"]];
            }
        }
        [imageDictionary setObject:imgArray forKey:category];
    }

#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"%@", imageDictionary);
#endif

}

#pragma mark - KKGridViewDataSource

- (NSUInteger)numberOfSectionsInGridView:(KKGridView *)gridView
{
    return [imgCategoriesArray count];
}

- (NSUInteger)gridView:(KKGridView *)gridView numberOfItemsInSection:(NSUInteger)section
{
    NSUInteger uint = 0;
    NSString *category = [imgCategoriesArray objectAtIndex:section];
    for (NSDictionary *item in resultsDictionary) {
        if ([category isEqualToString:[item objectForKey:@"Category"]]) {
            uint++;
        }
    }
    return uint;
}

- (CGFloat)gridView:(KKGridView *)gridView heightForHeaderInSection:(NSUInteger)section
{
    return kHeaderHeight;
}

- (KKGridViewCell *)gridView:(KKGridView *)gridView cellForItemAtIndexPath:(KKIndexPath *)indexPath
{
    KKGridViewCell *cell = [KKGridViewCell cellForGridView:gridView];
    
    NSString *img = [[[imageDictionary objectForKey:[imgCategoriesArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.index] stringByDeletingPathExtension];
    NSString *path = [[NSBundle mainBundle] pathForResource:img ofType:@"png"];
    cell.highlightAlpha = 0.3f;
    cell.imageView.image = [UIImage imageWithContentsOfFile:path];
    
    return cell;
    
}

- (NSString *)gridView:(KKGridView *)gridView titleForHeaderInSection:(NSUInteger)section
{
    //Return capitalized string of category name of section
    return [[imgCategoriesArray objectAtIndex:section] capitalizedString];
}

#pragma mark KKGridViewDelegate

- (void)gridView:(KKGridView *)gridView didSelectItemAtIndexPath:(KKIndexPath *)indexPath
{
    NSString *img = [[[imageDictionary objectForKey:[imgCategoriesArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.index] stringByDeletingPathExtension];
    NSString *path = [[NSBundle mainBundle] pathForResource:img ofType:@"png"];
    
    [self.delegate imageSelected:path];
    
    NSLog(@"Selected Item: %@", path);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end