//
//  ImageCategoryListViewController.h
//  Ques2Learn
//
//  Created by Kevin Vertucio on 09/9/13.
//
//

#import <UIKit/UIKit.h>
#import "ImageGridViewController.h"

@protocol ImageCategoryViewControllerDelegate <NSObject>

- (void)imageSelected:(NSString *)imagePath;

@end

@interface ImageCategoryListViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate,ImageGridViewControllerDelegate>

@property (assign) id <ImageCategoryViewControllerDelegate> delegate;

@end
