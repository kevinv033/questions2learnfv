//
//  ImageGridViewController.h
//  Ques2Learn
//
//  Created by Kevin Vertucio on 09/9/13.
//
//

#import <KKGridView/KKGridViewController.h>


@protocol ImageGridViewControllerDelegate <NSObject>

- (void)imageSelected:(NSString *)image;

@end

@interface ImageGridViewController : KKGridViewController

@property (assign) id <ImageGridViewControllerDelegate> delegate;
@property (strong) NSString *category;
@property (strong) NSArray *imageArray;

@end
