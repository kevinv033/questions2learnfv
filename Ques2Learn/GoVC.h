//
//  GoVC.h
//  Ques2Learn
//
//  Created by apple on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupVC.h"
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
@interface GoVC : UIViewController <AVAudioPlayerDelegate,GroupVCDelegate, UIAlertViewDelegate>{
    AVAudioPlayer *audioPlayer;
    GroupVC *viewGroupVC;
    IBOutlet UITextView *lblQue;
    NSMutableArray *arrQue;
    NSMutableArray *arrStud; 
    int countQue;
    IBOutlet UIView *viewFinsh;
    IBOutlet UIImageView *imgReinS;
    IBOutlet UILabel *lblAnsT1;
    IBOutlet UILabel *lblAnsT2;
    IBOutlet UILabel *lblAnsT3;
    IBOutlet UILabel *lblAnsT4; 
    IBOutlet UILabel *lblAnsT5;
    //Top Barbone
    IBOutlet UIImageView *imgView1;
    IBOutlet UIImageView *imgView2;
    IBOutlet UIImageView *imgView3;
    IBOutlet UIImageView *imgView4;
    IBOutlet UIImageView *imgView5;
    IBOutlet UIImageView *imgView6;
    IBOutlet UIImageView *imgView7;
    IBOutlet UIImageView *imgView8;
    IBOutlet UIImageView *imgView9;
    IBOutlet UIImageView *imgView10;//..
    //Bottom Students..
    IBOutlet UIButton *btnStudent1;
    IBOutlet UIButton *btnStudent2;
    IBOutlet UIButton *btnStudent3;
    IBOutlet UIButton *btnStudent4;
    IBOutlet UILabel *lblStudent1;
    IBOutlet UILabel *lblStudent2;
    IBOutlet UILabel *lblStudent3;
    IBOutlet UILabel *lblStudent4;//..
    //Tag to get index
    int selectedOption;//Selected Answer
    int selectStudent;//Selectd student
    int selectLevel;//Selected Level
    int tagReinSch;//Rein Schedule
    int tagVPrompt;
    int cntCorrect;
    int cntCorrect1;
    int tagReinI;
    int tagReinType;
    int tagAFB;
    int tagSV;
//    int tagSOrN;
    DAL *objDAL;
    NSMutableArray *arrRecordQueNo;//Record Que. No.
    int tagView;
    IBOutlet UIButton *btnPOS;
    int tagQue;
//    NSMutableDictionary *dictQue;
    NSMutableDictionary *dicMQ;
    NSMutableDictionary *dictQue5;
    int tagSOrN5; 
    NSMutableArray *arrMVScore;
    
    IBOutlet UIImageView *imgBG1;
    IBOutlet UIImageView *imgBG2;
    IBOutlet UIImageView *imgBG3;
    IBOutlet UIImageView *imgBG4;
    IBOutlet UIImageView *imgBG5;
}
@property (nonatomic, retain) GroupVC *viewGroupVC;

-(void)getViewTag:(int)viewTag;
-(IBAction)clickHome:(id)sender;
-(IBAction)clickHelp:(id)sender;
-(void)clickSettings;
-(IBAction)clickLoadQuestion:(id)sender;
-(UIImage*)loadImage:(NSString *)imgN;
-(void)loadDict;
-(IBAction)clickSelectOption:(id)sender;
-(NSString*)checkForCustomOrModify:(NSString*)strField:(NSMutableDictionary*)dc;
-(IBAction)chooseStudent:(id)sender;
-(void)setNilHighLighedImage;
-(void)loadQuestion;
-(void)showReinforce;
-(NSMutableArray*)generateQue:(NSMutableArray*)arrAllQue:(int)noOfQue;
-(NSMutableArray*)getSelectQue:(NSString*)catStr:(NSString*)typeStr:(int)tag:(int)nmStr;
-(int)generateRandomNumber:(int)cntA;
-(IBAction)clickPlayOrStop:(id)sender;
-(int)checkForCorrectAnswer;
-(BOOL)checkCorrectOrNotAnswer;
-(void)saveScore;
-(void)audioFeedback;
-(void)loadAllBone;
-(void)showReinforce1;
-(void)audioFeedback1;
-(void)wrongSelectionReinforce;
-(NSMutableArray *)makeRecordString;
-(void)updateScore;
-(void)loadBlankView;
-(void)manageAttemptCount:(int)optSelect:(BOOL)yOrN;
@end
