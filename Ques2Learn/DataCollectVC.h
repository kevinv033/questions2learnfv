//
//  DataCollectVC.h
//  Ques2Learn
//
//  Created by apple on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectStudVC.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "SelectRecordVC.h"
@interface DataCollectVC : UIViewController <SelectStudVCDelegate,MFMailComposeViewControllerDelegate,SelectRVCDelegate>{
    NSString *strStudID;
    DAL *objDAL;
    SelectStudVC *viewSelectStudVC;
    SelectRecordVC *viewRVC;
    IBOutlet UIScrollView *scrVSList;
    NSMutableArray *arrSDetail;
    NSMutableArray *arrGet;
    IBOutlet UITableView *tblSRecords;
    int tagS;
    NSString *strName;
    int tagP1M1;
    UIPrintInteractionController *printController;
    
    CGSize pageSize;
}
@property (nonatomic, retain) SelectRecordVC *viewRVC;
@property (nonatomic, retain) SelectStudVC *viewSelectStudVC;
@property (nonatomic, retain) UINib *tableCellLoader;

//- (BOOL) isconnectedToNetwork;
-(void)reloadTbl:(NSMutableArray*)arr;
- (void)joinPDF;
-(void)createPDFfromUIView:(UIImageView*)aView saveToDocumentsWithFileName:(NSString*)aFilename;
-(void)makeScorePDF;
- (void)printTapped;
-(void)SendMail;
-(void)getStudID:(NSString*)idStr;
-(void)getStudIDFrmMain:(NSString*)idStr:(NSMutableArray*)scoreArr;
-(IBAction)clickHome:(id)sender;
-(IBAction)clickBack:(id)sender;
-(void)clickStud:(id)sender;
-(IBAction)clickShare:(id)sender;
-(void)saveScore:(NSString*)strCon:(NSMutableDictionary*)dicRecord:(NSString*)strID;
-(void)updateScore:(NSString*)strCon:(NSMutableDictionary*)dicRecord:(NSString*)strID;
- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx;
@end
