//
//  SelectSVC.m
//  Ques2Learn
//
//  Created by apple on 5/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SelectSVC.h"

@implementation SelectSVC
@synthesize delegate = _delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    objDAL = [[DAL alloc] initDatabase:@"Que2learn.sqlite"];
    arrStud = [[NSMutableArray alloc] init];
    arrStud = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:[NSString stringWithFormat:@"StudSettings where StudID<>%@",strID]]];
    if ([arrStud count]>0) {
        arrGroup = [[NSMutableArray alloc] init];
        arrCollect = [[NSMutableArray alloc] init];
        for (int i=0; i<[arrStud count]; i++) {
                [arrCollect addObject:@"NO"];
        }
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No student created" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [arrStud count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    NSString *strSN = [[arrStud objectAtIndex:indexPath.row] valueForKey:@"StudPhoto"];
    if ([strSN isEqualToString:@"OK.png"]) {
    }else {
        NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsDir = [dirPaths objectAtIndex:0];
        NSString *imgFilePath = [docsDir stringByAppendingPathComponent:strSN];
        UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
        if (imgGet == nil) {
            
        }else {
            cell.imageView.image = imgGet;
        }
    }
    if ([[arrCollect objectAtIndex:indexPath.row] isEqualToString:@"YES"]) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    } 
    cell.textLabel.text = [[arrStud objectAtIndex:indexPath.row] valueForKey:@"StudName"];
    return cell;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([[tableView cellForRowAtIndexPath:indexPath] accessoryType] == UITableViewCellAccessoryCheckmark){
        [arrGroup removeObject:[arrStud objectAtIndex:indexPath.row]];
        [arrCollect replaceObjectAtIndex:indexPath.row withObject:@"NO"];
    }
    else {
        [arrGroup addObject:[arrStud objectAtIndex:indexPath.row]];
        [arrCollect replaceObjectAtIndex:indexPath.row withObject:@"YES"];
    }
    [tblS reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark -
#pragma mark Custom methods
-(void)getStudID:(NSString*)idStr
{
    strID = idStr;
}
-(void)clickDone:(id)sender
{
//    if ([arrStud count]>0 && strID==0) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Select student for data collected"   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];        
//        return;
//    }
    [self dismissModalViewControllerAnimated:YES];
    [_delegate selectStudsDone:[[arrGroup valueForKey:@"StudID"] componentsJoinedByString:@","]];
}
@end
