//
//  OptionVC1.m
//  Ques2Learn
//
//  Created by apple on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OptionVC1.h"

@implementation OptionVC1
@synthesize arrStudents = _arrStudents;
@synthesize delegate = _delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *btnDelete=[UIButton buttonWithType:UIButtonTypeCustom];
	[btnDelete addTarget:self action:@selector(clickDone1:)forControlEvents:UIControlEventTouchDown];
	[btnDelete setImage:[UIImage imageNamed:@"btnDone.png"] forState:UIControlStateNormal];
	btnDelete.frame = CGRectMake(230, 10, 50, 26);
	[self.view addSubview:btnDelete];
    tblOptions = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, 285, 132) style:UITableViewStylePlain];
    tblOptions.delegate =self;
    tblOptions.dataSource=self;
    tblOptions.scrollEnabled = YES;
 
	[self.view addSubview:tblOptions];
	[self.view	setBackgroundColor:[UIColor clearColor]]; 
    arrGroup = [[NSMutableArray alloc] init];
    arrCollect = [[NSMutableArray alloc] init];
    for (int i=0; i<[_arrStudents count]; i++) {
        [arrCollect addObject:@"NO"];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma
#pragma mark - UITableView Delegate mathods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_arrStudents count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.textLabel.text = [_arrStudents objectAtIndex:indexPath.row];
    cell.textLabel.font=[UIFont boldSystemFontOfSize:18];
    UIImageView *imgSelect = [[UIImageView alloc] init];
    imgSelect.frame = CGRectMake(250, 14.5, 15, 15);
    imgSelect.backgroundColor = [UIColor clearColor];
	if([[arrCollect objectAtIndex:indexPath.row] isEqualToString:@"YES"]){
        
        imgSelect.image = [UIImage imageNamed:@"check@2x.png"];
	}
	else {
        imgSelect.image = [UIImage imageNamed:nil];
	}
    [cell.contentView addSubview:imgSelect];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self clickSelectRow:indexPath.row];
}
#pragma
#pragma mark - custom methods
-(void)clickDone1:(id)sender
{
//    NSLog(@"arrGroup:%@",arrGroup);
    if ([arrGroup count]==[_arrStudents count]) {
        [_delegate rowOptionSelected1:[arrGroup objectAtIndex:0]];
    }else if ([arrGroup count]==[_arrStudents count]){
        [_delegate rowOptionSelected1:[arrGroup objectAtIndex:0]];
    }else{
        [_delegate rowOptionSelected1:[arrGroup componentsJoinedByString:@","]];
    }
}

-(void)clickSelectRow:(int)sender
{
    //    UIButton *btn = sender;
    if ([[arrCollect objectAtIndex:sender] isEqualToString:@"YES"]) {
        if (sender!=0) {
            [arrGroup removeObject:[_arrStudents objectAtIndex:0]];
            [arrCollect replaceObjectAtIndex:0 withObject:@"NO"];
        }
        [arrGroup removeObject:[_arrStudents objectAtIndex:sender]];
        [arrCollect replaceObjectAtIndex:sender withObject:@"NO"];
    }else {
        if (sender==0) {
            [arrGroup removeAllObjects];
            for (int i=0; i<[_arrStudents count]; i++) {
                [arrGroup addObject:[_arrStudents objectAtIndex:i]];
                [arrCollect replaceObjectAtIndex:i withObject:@"YES"];
            }
        }else{
            [arrGroup addObject:[_arrStudents objectAtIndex:sender]];
            [arrCollect replaceObjectAtIndex:sender withObject:@"YES"];
        }
    }
    [tblOptions reloadData];
}
-(void)getOptions:(NSMutableArray*)arrGet:(NSInteger)optTag
{
    tagOpt = optTag;
    self.arrStudents = [NSMutableArray array];
    _arrStudents = [NSMutableArray arrayWithArray:arrGet];
}
@end
