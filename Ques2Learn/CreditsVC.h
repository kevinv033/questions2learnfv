//
//  CreditsVC.h
//  Ques2Learn
//
//  Created by apple on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
@interface CreditsVC : UIViewController <MFMailComposeViewControllerDelegate> {
    IBOutlet UIView *viewAHTU;
    IBOutlet UIView *viewAHTU1;
    IBOutlet UIView *viewC;
    IBOutlet UIWebView *webViewTC;
    IBOutlet UIWebView *webViewTC1;
    IBOutlet UIButton *btnAb;
    IBOutlet UIButton *btnHTU;
    IBOutlet UIButton *btnCS;
}
-(void)loadDocument:(NSString*)documentName inView:(UIWebView*)webView;
-(IBAction)clickHome:(id)sender;
-(IBAction)clickAbout:(id)sender;
-(IBAction)clickHowToUse:(id)sender;
-(IBAction)clickHelp:(id)sender;
-(IBAction)clickForHelp:(id)sender;
-(IBAction)clickOpenURL:(id)sender;
-(IBAction)clickOpenURLDes:(id)sender;
-(IBAction)clickOpenURLVideo:(id)sender;
@end
