//
//  ImageSelectViewController.h
//  Ques2Learn
//
//  Created by Kevin Vertucio on 09/6/13.
//
//

#import <KKGridView/KKGridViewController.h>

@protocol ImageSelectViewControllerDelegate <NSObject>

- (void)imageSelected:(NSString *)imagePath;

@end

@interface ImageSelectViewController : KKGridViewController

@property (assign) id <ImageSelectViewControllerDelegate> delegate;

@end
