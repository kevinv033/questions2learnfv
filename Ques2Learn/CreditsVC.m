//
//  CreditsVC.m
//  Ques2Learn
//
//  Created by apple on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CreditsVC.h"
#import "HelpVC.h"
@implementation CreditsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    btnAb.layer.cornerRadius=3;
    btnAb.layer.borderWidth=2;
    btnCS.layer.cornerRadius=3;
    btnCS.layer.borderWidth=2;
    btnHTU.layer.cornerRadius=3;
    btnHTU.layer.borderWidth=2;
    btnAb.backgroundColor=[UIColor whiteColor];
    btnCS.backgroundColor=[UIColor whiteColor];
    btnHTU.backgroundColor=[UIColor whiteColor];
    [self clickAbout:nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - MFMailComposeViewController delegate methods
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	[self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - custom methods
-(void)clickOpenURL:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.speechpups.com"]];
}
-(void)clickOpenURLDes:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.montecreative.com"]];
}
-(void)clickOpenURLVideo:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.speechpups.com/training"]];
}
-(void)clickHome:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)loadDocument:(NSString*)documentName inView:(UIWebView*)webView
{
    NSString *path = [[NSBundle mainBundle] pathForResource:documentName ofType:nil];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    webView=nil;
    request=nil;
    
}
-(void)clickAbout:(id)sender
{    
    btnAb.layer.borderColor=[UIColor darkTextColor].CGColor;
    btnCS.layer.borderColor=[UIColor lightGrayColor].CGColor;
    btnHTU.layer.borderColor=[UIColor lightGrayColor].CGColor;
    btnAb.layer.masksToBounds=YES;
    btnCS.layer.masksToBounds=NO;
    btnHTU.layer.masksToBounds=NO;
    viewC.hidden=YES;
    viewAHTU.hidden=NO;
    viewAHTU1.hidden=YES;
    [self loadDocument:@"app writeup_0.docx" inView:webViewTC];
}
-(void)clickHowToUse:(id)sender
{
    btnAb.layer.borderColor=[UIColor lightGrayColor].CGColor;
    btnCS.layer.borderColor=[UIColor lightGrayColor].CGColor;
    btnHTU.layer.borderColor=[UIColor darkTextColor].CGColor;
    btnAb.layer.masksToBounds=NO;
    btnCS.layer.masksToBounds=NO;
    btnHTU.layer.masksToBounds=YES;
    viewC.hidden=YES;
    viewAHTU.hidden=YES;
    viewAHTU1.hidden=NO;
    [self loadDocument:@"How to use Q2L app info.docx" inView:webViewTC1];
}
-(void)clickHelp:(id)sender
{
    btnAb.layer.borderColor=[UIColor lightGrayColor].CGColor;
    btnCS.layer.borderColor=[UIColor darkTextColor].CGColor;
    btnHTU.layer.borderColor=[UIColor lightGrayColor].CGColor;
    btnAb.layer.masksToBounds=NO;
    btnCS.layer.masksToBounds=YES;
    btnHTU.layer.masksToBounds=NO;
    viewC.hidden=NO;
    viewAHTU.hidden=YES;
    viewAHTU1.hidden=YES;
}
-(void)clickForHelp:(id)sender
{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setToRecipients:[NSArray arrayWithObject:@"leanne@speechpups.com"]];
        [mailViewController setSubject:@""];
        [mailViewController setMessageBody:@"" isHTML:NO];
        [[mailViewController navigationBar] setBarStyle:UIBarStyleBlackOpaque];
        [self presentViewController:mailViewController animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You are not logged in your device email account." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [alert show];
    }
}
@end
