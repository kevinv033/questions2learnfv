//
//  CustomQueVC.m
//  Ques2Learn
//
//  Created by apple on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomQueVC.h"
#import "CreateNewCatVC.h"
#import "ModifyPreProgVC.h"

@interface CustomQueVC ()
{
    UIPopoverController *popOver;
    NSString *theCategorytoDelete;
}

@end

@implementation CustomQueVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    objDAL = [[DAL alloc] initDatabase:@"Que2learn.sqlite"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"select * from StudSettings where StudID='%@'",strStudID]]];

    if ([dict count]>0) {
        lblStudName.text=[[dict valueForKey:@"Table1"] valueForKey:@"StudName"];
        NSString *strSN = [[dict valueForKey:@"Table1"] valueForKey:@"StudPhoto"];
        if ([strSN isEqualToString:@"OK.png"]) {
        }else {
            NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *docsDir = [dirPaths objectAtIndex:0];
            NSString *imgFilePath = [docsDir stringByAppendingPathComponent:strSN];
            UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
            if (imgGet == nil) {
                
            }else {
                [btnSImg setBackgroundImage:imgGet forState:UIControlStateNormal];
            }
        }
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
#pragma mark-
#pragma mark custom methods
-(NSArray*)getCategories
{
//  Get all Customized categories
    NSMutableDictionary *qcatsDict = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:@"select * from tblCustom"]];
    NSMutableArray *qcats = [NSMutableArray arrayWithCapacity:0];
    NSEnumerator *qcatEnumerator = [qcatsDict objectEnumerator];
    id obj;
    while (obj = [qcatEnumerator nextObject]) {
        //      Get the category
        NSString *category = [obj objectForKey:@"QCat"];
        //      Add Qcat to array unless it already exists
        if (![qcats containsObject:category]) {
            [qcats addObject:category];
        }
    }
    return [qcats copy];
}

-(void)getStudID:(NSString*)idStr
{
    strStudID = idStr;
}
-(void)clickHome:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)clickCNC:(id)sender
{
    CreateNewCatVC *viewController = [[CreateNewCatVC alloc] initWithNibName:@"CreateNewCatVC" bundle:nil];
    [viewController getStudID:strStudID];
    [self.navigationController pushViewController:viewController animated:YES];
}
-(IBAction)clickMPAC:(id)sender
{
    ModifyPreProgVC *viewController = [[ModifyPreProgVC alloc] initWithNibName:nil bundle:nil];
    [viewController getStudID:strStudID];
    [self.navigationController pushViewController:viewController animated:YES];
}
-(void)clickManageCustomCat:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    [self getCategories]; //Get the current list of categories
    ManageCategoriesViewController *mvc = [ManageCategoriesViewController new];
    mvc.categories = [self getCategories];
    mvc.delegate = self;
    popOver = [[UIPopoverController alloc] initWithContentViewController:mvc];
    popOver.delegate = self;
    [popOver presentPopoverFromRect:btn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}
#pragma mark - ManageCategoriesDelegate
-(void)selectCategoryForDeletion:(NSString*)category
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete a Category" message:[NSString stringWithFormat:@"Are you sure you want to delete %@?",category] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes",nil];
    alert.tag = 111;
    alert.delegate = self;
    [alert show];
    theCategorytoDelete = category;
    
//    [objDAL deleteFromTable:@"tblCustom" WhereField:@"QCat=" Condition:category];
}
#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [objDAL executeDataSet:[NSString stringWithFormat:@"delete from tblCustom where QCat='%@'",theCategorytoDelete]];
        [popOver dismissPopoverAnimated:YES];
    }else{
        return;
    }
}

@end

