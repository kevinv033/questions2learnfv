//
//  MainViewAppDelegate.h
//  Ques2Learn
//
//  Created by apple on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MainViewViewController;
@interface MainViewAppDelegate : UIResponder <UIApplicationDelegate,UINavigationControllerDelegate>{
    UIWindow *window;
    NSString *dbPath;
    id obj;
}
@property (strong, nonatomic)id obj;
@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (strong, nonatomic) IBOutlet UINavigationController *navController;
-(void)checkAndCreateDatabase;
@end
//- (BOOL)shouldAutorotate
//{
//    return NO;
//}
//com.speechpups.QuestionToLearnPaid