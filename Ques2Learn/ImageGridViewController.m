//
//  ImageGridViewController.m
//  Ques2Learn
//
//  Created by Kevin Vertucio on 09/9/13.
//
//

#import "ImageGridViewController.h"
#import <KKGridView/KKGridView.h>
#import <KKGridView/KKGridViewCell.h>
#import <KKGridView/KKIndexPath.h>
#import <JSONKit/JSONKit.h>

@interface ImageGridViewController ()
{
    NSMutableArray *imgCategoriesArray;
}

@end

@implementation ImageGridViewController

- (void)loadView
{
    [super loadView];
	// Do any additional setup after loading the view, typically from a nib.
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"Image Array:%@", _imageArray);
#endif
    self.gridView.cellSize = CGSizeMake(95.0f,95.0f);
    self.title = [_category capitalizedString];
    
}
#pragma mark - KKGridViewDataSource

- (NSUInteger)numberOfSectionsInGridView:(KKGridView *)gridView
{
    return 1;
}

- (NSUInteger)gridView:(KKGridView *)gridView numberOfItemsInSection:(NSUInteger)section
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"%i", [_imageArray count]);
#endif
    return [_imageArray count];
}

- (CGFloat)gridView:(KKGridView *)gridView heightForHeaderInSection:(NSUInteger)section
{
    return 0;
}

- (KKGridViewCell *)gridView:(KKGridView *)gridView cellForItemAtIndexPath:(KKIndexPath *)indexPath
{
    KKGridViewCell *cell = [KKGridViewCell cellForGridView:gridView];
    
    NSString *img = [_imageArray objectAtIndex:indexPath.index];
    cell.highlightAlpha = 0.3f;
    cell.imageView.image = [UIImage imageNamed:img];
    
    return cell;
    
}

- (NSString *)gridView:(KKGridView *)gridView titleForHeaderInSection:(NSUInteger)section
{
    //Return capitalized string of category name of section
    return nil;
}

#pragma mark KKGridViewDelegate

- (void)gridView:(KKGridView *)gridView didSelectItemAtIndexPath:(KKIndexPath *)indexPath
{
    NSString *img = [_imageArray objectAtIndex:indexPath.index];

#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"Selected Item: %@", img);
#endif

    [[self.navigationController.viewControllers objectAtIndex:0] imageSelected:img];
    [self.navigationController popToRootViewControllerAnimated:YES];
    

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
