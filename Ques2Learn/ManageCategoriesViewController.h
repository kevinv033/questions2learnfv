//
//  ManageCategoriesViewController.h
//  Ques2Learn
//
//  Created by Kevin Vertucio on 01/3/14.
//
//

#import <UIKit/UIKit.h>

@protocol ManageCategoriesDelegate <NSObject>

-(void)selectCategoryForDeletion:(NSString*)category;

@end

@interface ManageCategoriesViewController : UITableViewController

@property (nonatomic, assign) id<ManageCategoriesDelegate> delegate;
@property (nonatomic, retain) NSArray *categories;

@end
